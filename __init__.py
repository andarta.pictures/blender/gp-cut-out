# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.



bl_info = {
    "name" : "gp-cut-out",
    "author" : "Tom Viguier",
    "description" : "",
    "blender" : (3, 3, 0),
    "version" : (0, 9, 3),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}

import bpy
from .common.import_utils import ensure_lib

optional_lib= [
    ['PIL','pillow'],
    ]
for lib in optional_lib :
    has_lib = ensure_lib(lib[0],lib[1])
    if not has_lib:
        print("Warning: Could not import nor install " + lib[0] +'\n'
              + 'Some features may not work properly \n' +              
                'Try again and if this error persist install dependencies manually')


from bpy.props import BoolProperty, IntProperty,EnumProperty, FloatProperty, StringProperty, CollectionProperty, IntVectorProperty, PointerProperty
from bpy.types import PropertyGroup
from .common.gp_utils import frame_confo
# from .common.keymaps import *
from .common.render_utils import get_viewport_gif, GPCO_OT_GENERATE_ASSETS_GIFS, render_image_sequence
from .common_functions import *
from .Importers import *
from .Asset_management import *
import rna_keymap_ui

current_version = bl_info.get("version")

'''------------------------PROPERTIES------------------------'''
class root_objs_prop(PropertyGroup):
    bl_label = "Asset root objects"
    bl_idname = "asset.rootobjs"
    object : PointerProperty(type = bpy.types.Object)
   
    
class ASSET_PROPS(PropertyGroup):
    bl_label = "Asset properties"
    bl_idname = "asset.properties"
    asset_type : EnumProperty(items=[('NONE', 'None', 'None', 0), 
                                     ('POSE', 'Pose', 'Simple Pose', 1), 
                                     ('ANIM', 'Animation','Animation template',2),
                                     ('MC', 'Master Controller', 'Master controller template', 3)],
                                      default = 'NONE'
                                    )
    MC : BoolProperty(default = True)
    range : IntVectorProperty(size = 2, min = 1)
    grid : IntVectorProperty(size = 2, min = 1)
    src_scene : PointerProperty(type = bpy.types.Scene)
    # root_objects : CollectionProperty(type = root_objs_prop)
    # root_objects :  PointerProperty(type = bpy.types.Action.asset_root_objects)
    asset_empty :  PointerProperty(type = bpy.types.Object)
    name : StringProperty()
    description : StringProperty()
    
    possible_roots : StringProperty()
    selected_roots : StringProperty()
    preview_path : StringProperty()
    catalog_id : StringProperty()
  

    def get_root_enum(self,context ):
        items = []
        i = 0
        names = self.possible_roots.split(',')
        for n in names :
                items.append(( n, n, "Mesh %s" % n ,i))
                i += 1
        return items  
        
    # TODO remove when string search is OK
    def update_root_enum(self,context):
        #add current root_enum to selected_root
        if self.roots_enum == 'NONE' :
            return
        elif self.roots_enum == 'REMOVE LAST' :
            self.remove_last_from_selected_root()
            return
        else:
            self.add_selected_root()
            
    roots_enum : EnumProperty(items=get_root_enum, update = update_root_enum,
                               description = 'pick an object to add it to root list, pick REMOVE LAST to remove the last one',
                              )


    def add_searched_root(self,value):
        match value:
            case 'CLEAR':
                self.set_selected_roots('',addition=False)
                return
            case 'REMOVE LAST' :
                self.remove_last_from_selected_root()
                return
            case _:
                string = value+','
                self.set_selected_roots(string,addition=True)
                #remove string from possible_roots
                self["possible_roots"] = self.possible_roots.replace(string, '')

    roots_enum_string: bpy.props.StringProperty(
        description='pick an object to add it to root list, pick REMOVE LAST to remove the last one',
        default = '',
        search=lambda self, context, edit_text: ['CLEAR','REMOVE LAST']+[obj.name for obj in context.scene.objects 
                                                                        if (obj.type == 'GPENCIL' 
                                                                            and obj.name not in self.get_selected_roots_names()
                                                                            )],
        set = add_searched_root
    )

    def add_selected_root(self):
        string = self.roots_enum+','
        self.set_selected_roots(string,addition=True)
        #remove string from possible_roots
        self["possible_roots"] = self.possible_roots.replace(string, '')

    def set_possible_roots(self, string):
        self["possible_roots"] = 'NONE,REMOVE LAST,'+string

    def set_selected_roots(self, string,addition = False):
        if not addition and string == '':
            self["selected_roots"] = 'no root selected!'
            return
        elif addition and self.selected_roots != 'no root selected!':
            self["selected_roots"] += string
        else:
            self["selected_roots"] = string
            
    def get_selected_roots_names(self):
        #return list of root names
        if self.selected_roots == '' or self.selected_roots == 'no root selected!':
            return []
        else:
            return self.selected_roots.split(',')[:-1]
            
    def remove_last_from_selected_root(self):
        #remove last root_enum from selected_root
        string = self.selected_roots.split(',')[-2]+','
        self.set_selected_roots(self.selected_roots.replace(string, ''))
        #add string to possible_roots
        self["possible_roots"] += string

    def sanity_check(self,context,update_root = True,enforce_kf = False, update_src_name = False):
            data  = self
            #WIP : sanity check : keyframe every frame / special characters / libraries
            print('Asset Sanity checking')
            # objects_list = get_objects_and_childs(context.selected_objects, context.scene.objects)
            #check roots
            
            root_object_names = data.get_selected_roots_names()
            if len(root_object_names) == 0 :
                print('error no root selected')
                return False
                
            root_list = []
            for name in root_object_names :
                if not self.src_scene.objects.get(name) :
                    print('error root not found')
                    return False
                    
                else :
                    root_list.append(self.src_scene.objects[name])
            if update_root:
                self.root_objects_update(context,root_list)
            object_dic = get_objects_childs_dic(root_list, self.src_scene.objects,recursive_dic={})
            
            if data.asset_type != 'NONE' :
                

                #check range
                if data.asset_type != 'POSE' and data.range[0] >= data.range[1] :
                    print('error in range field')
                    return False
                    
                if data.asset_type == 'MC' :
                    #compare range and grid size 
                    if data.range[1] - data.range[0] + 1 != data.grid[0] * data.grid[1]:
                        print('error in range/grid correspondancy')  
                        return False
                        
                    #enforce keyframe every frame
                    if enforce_kf :
                        print('---enforce missing keyframes')
                        init_frame = self.src_scene.frame_current
                        for i in range(data.range[0], data.range[1] +1) :
                            self.src_scene.frame_set(i)
                            for k,object in object_dic.items() :
                                enforce_keyframe(object, i)
                                try:
                                    update_gp_points(object)
                                except:
                                    pass
                                
                        self.src_scene.frame_set(init_frame)
                        
                #enforce data names to match object names      
                # print('---match datablock names')                  
                # for k,object in object_dic.items() :   
                #     if object.data.name != object.name :
                #         object.data.name = object.name     
                #name the scene just like the blendfile
                print('---match blend file and scene name')
                #WARNING! to bypass once asset edit and read
                if update_src_name :
                    self.src_scene.name =  bpy.path.basename(context.blend_data.filepath)[:-6]              

            else :                
                print('error in asset type field')   
                return False

            return True  

    def copy_asset(self,context,src):
        #print('copy asset')
        self.name = src.name
        self.description = src.description
        self.asset_type = src.asset_type
        self.range = src.range
        self.src_scene=src.src_scene
        self.grid=src.grid
        self.selected_roots=src.selected_roots
        self.possible_roots=src.possible_roots
        self.catalog_id = src.catalog_id
        try:
            self.preview_path = src.preview_path
        except:
            print('no preview path')
     
    def update_asset_roots (self,context):
         #Holding root_objects pointers into collectionProperty
        if self.asset_empty:
            # clear self.asset_empty.animation_data.action.asset_root_objects            
            if self.asset_empty.animation_data.action.asset_root_objects :
                self.asset_empty.animation_data.action.asset_root_objects.clear()

            for j, name in enumerate(self.get_selected_roots_names()):
                obj = context.scene.objects.get(name)                
                self.asset_empty.animation_data.action.asset_root_objects.add()
                self.asset_empty.animation_data.action.asset_root_objects[j].object = obj

    def self_copy_to_asset_empty (self,context,asset_empty):
        #print('copy to empty')
        #rename asset_empty
        asset_empty.name = self.name
        #rename action
        asset_empty.animation_data.action.name = self.name

        #Copy temp import data to asset action
        import_data = asset_empty.animation_data.action.import_data
        import_data.copy_asset(context,self)        
        import_data.asset_empty = asset_empty
        #Holding root_objects pointers into collectionProperty
        import_data.update_asset_roots(context)

        # Mark the asset
        asset_empty.animation_data.action.asset_mark()
        asset_data = asset_empty.animation_data.action.asset_data
        asset_data.description = self.description
        asset_data.catalog_id = self.catalog_id

        # Generate thumbnails        
        if context.scene.frame_current not in range(self.range[0], self.range[1] +1):
            context.scene.frame_current = self.range[0]   

        # custom preview
        generate_custom_preview(context, asset_empty)

        # Generate a GIF
        #Iterate trough all frame of the range
        try:    
            path = get_viewport_gif(frame_range = (self.range[0],self.range[1]),
                                        prefix= self.name,
                                        render_path = bpy.path.abspath('//'),
                                        )
            import_data.preview_path = path
        except Exception as e:
            print('error while generating GIF:', e)
           
    def get_as_dictionary(self):
        result = {prop: getattr(self, prop) for prop in dir(self) if isinstance(getattr(self, prop), (str, int, bool, tuple, list))}
        result['grid'] = self.grid[:]
        result['range'] = self.range[:]
        return result
    
    def set_from_dictionary(self, dictionary, 
                            properties =  ['name', 'description', 'asset_type', 'range', 'grid', 'src_scene', 'selected_roots', 'possible_roots', 'catalog_id']):
        for prop in properties:
            if prop in dictionary and hasattr(self, prop):
                if prop in ['grid', 'range']:
                    setattr(self, prop, tuple(dictionary[prop]))
                else:
                    setattr(self, prop, dictionary[prop])


class import_properties(bpy.types.PropertyGroup):
    bl_label = "Asset import properties"
    bl_idname = "asset.impot_props"


    def set_import(self, value):
        self["hard_import"] = value

    hard_import : BoolProperty(name="Hard import", 
                               description="Import from scratch, even if objects already exist", 
                               default=False,
                               #set=set_import
                               )
    
    def set_flip(self,value):
        self['flip']=value

    flip : BoolProperty(name="Flip", description="Flip laeralized objects of the asset on import", default=False)

    use_current_collection : BoolProperty(name="In current collection", description="Import in current collection instead of recreating the whole tree", default=False)

    def set_so(self,value):
        self['selected_only']=value

    selected_only : BoolProperty(name="Selected only", description="Import only selected objects", default=False)
    hard_root : StringProperty(name="Hard root", description="Root object to import from", default="")
    possible_roots : StringProperty()
    version: StringProperty(name="Version", description="Version of the add-on", default=str(current_version))
    src_scene : StringProperty(name="Scene", description="source scene name", default=str(current_version))
    
    def get_root_enum(self,context ):
        items = []
        i = 0
        names = self.possible_roots.split(',')
        for n in names :
                items.append(( n, n, "Mesh %s" % n ,i))
                i += 1
        return items  
         
    roots_enum : EnumProperty(items=get_root_enum)

    


    
    def set_possible_roots(self, string):
        self["possible_roots"] = string


class GP_CUTOUT_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
    switch_depth_location : bpy.props.BoolProperty(default = True)
    depth_axis : bpy.props.EnumProperty(items = [('X', 'X', 'X', 0), ('Y', 'Y', 'Y', 1), ('Z', 'Z', 'Z', 2)], default = 'Y')
    interpolation_key : bpy.props.StringProperty(default = 'LEFT_SHIFT')
    flip_key : bpy.props.StringProperty(default = 'F')
    empty_scene_layout_path : bpy.props.StringProperty(default = '', subtype="FILE_PATH")
    use_subprocess : bpy.props.BoolProperty(default = True)

    def draw(self, context):
        layout = self.layout
        kc = bpy.context.window_manager.keyconfigs.addon
        col = layout.column()
        for km, kmi in addon_keymaps:
            km = km.active()
            col.context_pointer_set("keymap", km)
            rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)

        box = layout.box()
        row= box.row()
        row.label(text = 'Modal shortcut: ')
        row= box.row()
        row.label(text = 'Interpolation key : ')
        row.prop(self, 'interpolation_key', text = '')
        row= box.row()
        row.label(text = 'Flip key : ')
        row.prop(self, 'flip_key', text = '')
        box = layout.box()
        row = box.row()
        row.prop( self, 'switch_depth_location', text = 'clip location on depth axis' )
        if self.switch_depth_location :
            row = box.row()
            row.separator_spacer()
            row.prop(self, 'depth_axis')
        # Add file selector for empty_scene_layout_path
        box = layout.box()
        row = box.row()
        row.label(text = 'Empty file layout path:')
        row.prop(self, 'empty_scene_layout_path', text='')
        row = box.row()
        row.label(text = 'Use subprocess for import (mau avoid crash when using unstable addon aside GP_CUTOUT)' )
        row.prop(self, 'use_subprocess')

class OpenBrowser(bpy.types.Operator):
        bl_idname = "open.browser"
        bl_label = "Minimum code to open browser & get filepath"

        filepath = bpy.props.StringProperty(subtype="FILE_PATH") 
        #somewhere to remember the address of the file


        def execute(self, context):
            display = "filepath= "+self.filepath  
            print(display) #Prints to console  
            #Window>>>Toggle systen console

            return {'FINISHED'}

        def invoke(self, context, event): # See comments at end  [1]

            context.window_manager.fileselect_add(self) 
            #Open browser, take reference to 'self' 
            #read the path to selected file, 
            #put path in declared string type data structure self.filepath

            return {'RUNNING_MODAL'}  
            # Tells Blender to hang on for the slow user input

addon_keymaps = []           
default_keymaps = [
    {"label":"import with previous settings","name":"gpco.read_asset","space_type":'VIEW_3D', "default_km":"V", "region" : "3D View",
     'properties':{'force_draw':False}
     },
    #with V+alt
    {"label":"set & import","name":"gpco.read_asset","space_type":'VIEW_3D', "default_km":"V", "region" : "3D View", "alt" : True,
     'properties':{'force_draw':True}},
    ]
classes = [ import_properties,
            GPCUTOUT_OT_MCMODAL,
            GPCUTOUT_OT_IMPORT,
            ASSET_PROPS,
            GPCO_OT_READ_ASSET,
            root_objs_prop,             
            GPCO_OT_MARK_ASSET, 
            GPCO_OT_MOVE_ASSET,
            GP_CUTOUT_AddonPref,
            GPCO_OT_DELETE_ASSET,
            GPCO_OT_EDIT_ASSET,
            GPCO_OT_SELECT_MASTER,
            GPCO_OT_PREVIEW_ASSET,
            GPCO_OT_COMMAND_READ_ASSET,
            GPCO_OT_GENERATE_ASSETS_GIFS,
            GPCO_OT_MARK_SUBPROCESS,
            GPCO_OT_CREATE_EMPTY,
            GPCO_OT_PAPERCLIP_COPY,
            GPCO_OT_PAPERCLIP_PASTE,
            ]
def register():
    for cls in classes:
        try:
            bpy.utils.register_class(cls)
        except ValueError:
            print("class already registered")

    bpy.types.Scene.import_properties = bpy.props.PointerProperty(type=import_properties)    
    bpy.types.Scene.import_data = PointerProperty(type=ASSET_PROPS) 
    # DONE : move those properties over action type since actions are the real asset holder
    bpy.types.Action.import_data = PointerProperty(type=ASSET_PROPS) 
    bpy.types.Action.asset_root_objects = CollectionProperty(type = root_objs_prop)
    bpy.types.WindowManager.current_asset_action = PointerProperty(type=bpy.types.Action)
    items = [('NONE','None', 'None')]
    bpy.types.WindowManager.selected_asset_catalog = bpy.props.EnumProperty(
                                                                            name="Catalog",
                                                                            description="Choose a catalog",
                                                                            items=items
                                                                        )
    # Keymapping
    for default_keymap in default_keymaps:
        kc = bpy.context.window_manager.keyconfigs.addon 
        if kc :
            km = kc.keymaps.new(name=default_keymap['region'], 
                                space_type = default_keymap['space_type'])
            kmi = km.keymap_items.new(default_keymap['name'], 
                                      type = default_keymap['default_km'], 
                                      value = 'PRESS',
                                      alt = default_keymap.get('alt', False),   
                                      ctrl = default_keymap.get('ctrl', False),
                                      
                                      )
            for name, value in default_keymap.get('properties',{}).items():
                setattr(kmi.properties, name, value)

            kmi.active = True
            addon_keymaps.append((km, kmi))


    bpy.types.ASSETBROWSER_MT_context_menu.append(draw_import_context_menu)

def unregister():


    for cls in reversed(classes):
        try:
            bpy.utils.unregister_class(cls)
        except Exception as e:
            print(e)
    bpy.types.ASSETBROWSER_MT_context_menu.remove(draw_import_context_menu)
    del bpy.types.Action.import_data
    del bpy.types.Action.asset_root_objects
    del bpy.types.WindowManager.current_asset_action
    del bpy.types.WindowManager.selected_asset_catalog

    del bpy.types.Scene.import_data
    del bpy.types.Scene.import_properties
    
    #removing keymaps:
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

