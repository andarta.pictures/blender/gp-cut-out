# GP_Cut-Out

Create templates, then import poses or animations from asset browser in any scene.

Develloped on Blender 3.6, this add-on might not function with Grease pencil 3.0 refactor

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp-cut-out/-/archive/main/gp-cut-out-main.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User guide

- GP_Cut-Out is a tool that allow user to create library assets specific to their needs. It can be a pose template, an animation or even a grid Master Controller to be used in a similar way as the Toon Boom Master Controller grid.
    
-  # Asset creation : 
    From the asset browser context menu in current file mode, select 'create asset in current file' operator.

    ![context menu](docs/asset_browser_context_menu.png "Asset browser context menu") ![Asset creation menu](docs/Asset_creation_menu.png "Asset creation menu")
        
    - Asset types : 
        - POSE : Single pose asset, corresponding to a unique frame number in your template scene
        - ANIM : Animation asset, corresponding to a given frame range in the template scene. 
        - MASTER CONTROLLER : Master controllers are complex assets that will call a modal operator allowing you to select a pose depending on mouse position. It needs to specify a framge range and a grid size in order to map correcly the poses along mouse location. (note that grid size and frame range must match)

    ![Grid asset example](docs/MC_GRID.png "Grid asset example") ![MC creation menu](docs/MC_creation_menu.png "Master controller creation menu")

    - Root objects : Root objects is the higher object in the rig hierarchy that is concerned by the template, a lips asset should have the mouth or the face as root object so the whole body won't be impacted by importing it, a whole pose asset should have the master as root object in order to import the full pose. By default when your create an asset, the active object is picked as root object, but you can add more than on from the pick up list
    - Asset emptys : In order to hold the asset datas, the add-on creates an empty object in the 'Asset emptys collection'
    - GIF creation : At the template creation, a GIF export of the template is saved in the .blend folder so it can be accessed before importing a given template. It uses the active scene camera.
    - Keyframe enforcement : On Master controller creation, the addon enforces keyframes on every channel along the frame range due to the importer necessity to have a keyframe at every frame.
    
    - A demo file with a simple rig example is provided in the demo files folder : 
    ![Demo file](docs/demo_file.png "Demo file rig example")


-  # Importing Assets : 
    Set you asset librairy filepath to the folder where you saved the template files. You should now see the assets thumbnails in the asset browser editor
    - Select the asset you want to import and from the context menu, pick 'Import selected Asset' (default shortcut = <kbd>V</kbd>). On first import the following menu appears :

    ![context menu](docs/asset_browser_context_menu.png "Asset browser context menu") ![Import menu2](docs/import_menu_2.png "Import menu") ![Import menu](docs/import_menu.png "Import menu")

    - keep the scene linked : checking this option will keep the link with the source scene, allowing faster imports.
    - import from scratch : first import of a character, if unchecked, the add-on will look for the objects already present in the scene, if no match found the operator won't work. checking this option with the character already in the scene will result to a second instance of the character. Please note that on first import, it's highly recommended to import a single pose template (even if importing master cotrollers from scratch should work)
    - import in current collection : copy object/collections hierarchy starting in the active collection. if unchecked, the add-on will try to replicate the collection hierarchy of the source scene.
    - import selected only : imports on selected objects only (do not impact childrens)
    - import flipped asset : flip asset according to asset lateralization
    - Interpolate from current pose : for single pose assets only, runs a modal that allows you to interpolate between current character state and the template pose.
    - For the next imports, if the source scene is already linked, the add-on skips the menu for a smoother workflow. However you can call it back using the <kbd>Alt</kbd> + <kbd>V</kbd> shortcut

    - During the master controller modal you can use the following hotkeys as modifiers : <kbd>Left SHIFT</kbd> : interpolate between poses / <kbd>F</kbd> : Flip pose according to asset lateralization

- Asset lateralization : In order to allow users to import flipped poses the add-on relies on a naming convention identifying lateralized object (suffixed '_L' or '_R') and central objects (suffixed '_C')
    - lateralized object (suffixed '_L' or '_R') will be switched on flipped import (HAND_L pose will be imported on HAND_R object and so on)
    - central objects (suffixed '_C') will be flipped in edit mode equivalent
    - Objects with different suffixes or no suffixes, will be considered as asymmetrical and will be ignored during the flipped import

- Partial import : Depending on the active object and the asset root object, the add-on determines a local root object. The imported pose will be applied over the local root object childrens only, allowing to import part of a pose (ex turn a single limb), or import a pose that does'nt affects the whole rig (ex import a mouth shape without resetting the whole pose)

- Conventions : This tool relies on a few conventions in order to work as expected
    - Multi object rigs : characters or props are supposed to be split as different objects allowing to animate them and reorder them along the depth axis
    - Y depth axis : the add-on tests we ran assumes that every grease pencil drawing is done over the front X-Z plane, and ordered along the Y axis. However you can switch the depth axis in the addon preferences dialog.
    - Object mode animation : transforms of objects should all be done in object mode (no pose mode implemented)
    - Child-of constraint parentship : parenting objects with traditionnal method should also work but the worlkflow that is mostly implemented uses Child-of constraints with no inverse matrices. Please consider using the same method for better results.

- Preferences : 
 ![addon prefs](docs/addon_prefs.png "addon preferences") 
    - clip location on depth axis : If checked the add-on will not interpolate locations along the selected depth axis, trying to avoid depth fights. It mostly an 'all or nothing' approach.

# Warnings

This tool is still in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)