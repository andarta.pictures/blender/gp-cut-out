import shutil
import subprocess
import bpy 
import os
from pathlib import Path
from bpy.props import EnumProperty, BoolProperty, IntProperty, FloatProperty, StringProperty, IntVectorProperty,PointerProperty,CollectionProperty
from bpy.types import PropertyGroup
import time


from .common.gp_utils import get_now_gpframe
from .common.general_utils import reload_scene,create_empty_layout_scene
from .common.anim_utils import *
from .common_functions import *
from bpy_extras.io_utils import ImportHelper
import inspect
import tempfile

verbose = False


# To improve property mangment : https://blender.stackexchange.com/questions/77424/how-to-set-attributes-in-a-blender-registered-class-dynamically

def draw_import_context_menu(self, context):
    scene = context.scene

    #Check current library
    current_library_name = context.area.spaces.active.params.asset_library_ref
    selected_asset = context.asset_file_handle
    #asset absolute path
    
    isSource = False
    isLib = False
    if selected_asset :
        if current_library_name == 'LOCAL':
            #library path is current file path
            asset_path = Path(bpy.data.filepath).parent
            preview_path = os.path.join(asset_path, selected_asset.name+'.gif')
        else:
            library = context.preferences.filepaths.asset_libraries.get(current_library_name)
            library_path = Path(library.path) 
            asset_fullpath = library_path / selected_asset.relative_path
            #print("Asset file path : ", asset_fullpath)
            pathParts = str(asset_fullpath).split("\\")

            asset_path = ""
            for part in pathParts:
                if part.find(".blend") != -1:
                    break
                asset_path += part+"\\"
            
            preview_path = os.path.join(asset_path, selected_asset.name+'.gif')
        path = selected_asset.relative_path
        if path.find('.blend')==-1:
            isSource=True
        elif current_library_name != 'ALL':
            isLib = True

        # actionAsset = bpy.data.actions.get(selected_asset.name)        
        # if actionAsset:
        #     context.window_manager.current_asset_action = actionAsset
        #     data = context.window_manager.current_asset_action.import_data
        # else:
        #     data = None
    asset = bpy.context.asset_file_handle
    layout = self.layout
    layout.separator()
    row = layout.row()
    row.label(text = 'GP Cut_Out')
    #Paperclip operator
    row = layout.row()
    row.operator_context = 'INVOKE_DEFAULT'
    row.operator('gpco.copy_asset', text = 'Copy asset', emboss=True, depress = False)
    abs_dir = os.path.abspath(os.path.join(temp_dir, CLIPBOARD_FILE.lstrip("\\")))
    if os.path.exists( abs_dir):
        row = layout.row()
        row.operator_context = 'INVOKE_DEFAULT'
        row.operator('gpco.paste_asset', text = 'Paste asset', emboss=True, depress = False)

    if current_library_name == 'LOCAL':
        row = layout.row()
        row.operator_context = 'INVOKE_DEFAULT'
        op = row.operator('gpco.mark_asset', text = 'Create Assets in current file', emboss=True, depress = False)    
        op.is_external = False

    else:
        row = layout.row()
        row.operator_context = 'INVOKE_DEFAULT'
        op = row.operator('gpco.mark_asset', text = 'Create Assets in external file', emboss=True, depress = False)  
        op.is_external = True  
    
    if asset :
        #try to get asset import data        
            
        if isLib:
            row = layout.row()
            row.operator_context = 'INVOKE_DEFAULT'
            row.operator('gpco.read_asset', text = f'Import {asset.name}', emboss=True, depress = False,) 

        elif isSource and current_library_name == 'LOCAL':
            row = layout.row()
            row.operator_context = 'INVOKE_DEFAULT'
            op = row.operator('gpco.move_asset', text = f'Move {asset.name} in external file', emboss=True, depress = False)  
            row = layout.row()
            row.operator_context = 'INVOKE_DEFAULT'
            row.operator('gpco.del_asset', text = f' Delete {asset.name}', emboss=True, depress = False) 
            row = layout.row()
            row.operator_context = 'INVOKE_DEFAULT'
            row.operator('gpco.edit_asset', text = f' Edit {asset.name}', emboss=True, depress = False) 
        #build preview path (asset blender  scene path+.gif)
        # preview_path = os.path.join(os.path.dirname(asset.library.filepath), asset.name+'.gif')
        #check if preview exist
        row = layout.row()
        if preview_path!='' and os.path.exists(preview_path):
            # row = layout.row()
            row.operator_context = 'INVOKE_DEFAULT'
            #TODO open image with pil on mouse over              

            op = row.operator('gpco.preview_asset', text = 'open preview ', 
                            icon = 'FILE_IMAGE',
                            emboss=True, 
                            depress = False,
                            )
            op.path_preview = preview_path
        else:
                # row = layout.row()
                row.label(text = 'No preview available at '+preview_path, icon = 'ERROR')
    else:
        row = layout.row()
        row.label(text = 'To import, select an asset ', icon = 'ERROR')
    
def draw_asset_from_import_data(self,context):
    data  = context.scene.import_data
    layout = self.layout
    col=layout.column()
    layout.use_property_split = True
    split=layout.split()
    col = split.column()
    
    
    col.prop(data, 'name', text = 'Asset name ')
    col.prop(data, 'description', text = 'Description ')
    row = layout.row()

    row.prop(data, 'asset_type', text = 'Asset type ')

    row = layout.row()
    row.prop(context.window_manager, 'selected_asset_catalog', text= "Catalog :")

    if context.scene.camera :
        if data.asset_type != 'NONE' :
            box = layout.box()
            box.use_property_split = False
            row = box.row()
            row.prop(data, 'range', text = 'Frame range')
            if data.asset_type in ['ANIM', 'MC']:
                if data.range[1]<=data.range[0]:
                    data.range[1] = data.range[0]+1
                if data.asset_type == 'MC' :
                    row = box.row()
                    row.prop(data, 'grid', text = 'Grid size column x line')
                    
                    total_frame = data.range[1]-data.range[0] + 1
                    if data.grid[0]==1 or data.grid[1]==0:
                        data.grid[0] = total_frame
                        data.grid[1] = 1

                    if total_frame != data.grid[0] * data.grid[1] and total_frame % data.grid[0]!=0 : 
                        row.label(text= 'impossible!') 
                    else :
                        data.grid[1] = int(total_frame /data.grid[0])
            elif data.asset_type == 'POSE':
                if data.range[1]!=data.range[0]:
                    data.range[1] = data.range[0]
            row = layout.row()
            row.label(text= 'Selected roots :'+data.selected_roots)
            row = layout.row()
            # box = layout.box()            
            row.separator_spacer()
            # row.prop(data, 'roots_enum', text = 'add a root')
            row.prop(data, 'roots_enum_string', text = 'add a root')
            #add a button to call a 'data' function that will add the selected root to the list

            
            # for ob in context.selected_objects :
            #     row = box.row()
            #     row.label(text= ob.name)  
    else :
        row = layout.row()
        row.label(text = 'You need a camera to frame thumbnails', icon = 'ERROR')
        self.cancel = True    

# bpy.ops.gpco.command_read_asset(asset_source_scene = r'C:\Users\gerar\Documents\ANDARTA\GitRepo\gp-cut-out\test files\lib\EWI_CH_SALIM-EARTH_RIG_FULL_V084_ABSPATH_NO-MS.blend', 
# asset_name = '84_abs_back_pose')
'''------------------------OPERATORS------------------------'''
class GPCO_OT_COMMAND_READ_ASSET(bpy.types.Operator):
    '''An operator to import an asset from a command line:
    bpy.ops.gpco.command_read_asset(
        asset_source_scene = r'C:\Path\To\File\File.blend', 
        asset_name = 'The_Asset_Name',
        root_name = 'The_Root_Object_Name'(optional)
        )
    args : asset_source_scene = the absolute path to the source scene cannot be None
           asset_name = the name of the asset to import cannot be None
           root_name = the name of the root object to import from. If None, the first root object of the asset will be used
           '''
    bl_idname = "gpco.command_read_asset"
    bl_label = "Command import asset"         
    bl_options = {'REGISTER', 'UNDO'}
    keep_linked : BoolProperty(default=True)
    hard_import : BoolProperty(default=True)
    asset_source_scene : StringProperty(     
        default = 'None',
        description = 'the name of the scene where the asset is located',
        subtype='FILE_PATH',
    )
    asset_name : StringProperty(     
        default = 'None',
        description = 'the name of the asset to import'
    )
    root_name : StringProperty(
        default = 'None',
        description = 'the name of the root object to import from'
    )

    def execute(self, context):
        prop  = context.scene.import_properties
        prop.hard_import = self.hard_import
        prop.selected_only = False
        prop.flip = False
        prop.use_current_collection = True
        prop.src_scene = self.asset_source_scene

        #if self.asset_source_scene in [lib.filepath for lib in bpy.data.libraries]:
        #    print(f"Scene {self.asset_source_scene} already linked : reloading")
        #    src_lib = [lib for lib in bpy.data.libraries if lib.filepath == self.asset_source_scene][0]
        #    src_lib.reload()
        #else :
        try:
            with bpy.data.libraries.load(self.asset_source_scene, link=True) as (data_from, data_to):
                    data_to.scenes = data_from.scenes
                    print("scene ", self.asset_source_scene, " has been linked")
                    self.is_linked = True
        except:
            print('CANCEL: scene %s not found'%self.asset_source_scene)
            return{'CANCELLED'}
        # Get the corresponding action in the linked scene
        #asset = bpy.data.actions.get(self.asset_name)
        asset = None
        for action in bpy.data.actions:
        
            #if it hasn't import_data attribute Pass
            if not hasattr(action, 'import_data'):
                continue
            if action.library and action.library.filepath[2:] in self.asset_source_scene:
                if action.name == self.asset_name:
                    asset = action
                    break



        if not asset:
            print('CANCEL: asset %s not found'%self.asset_name)
            #print all available assets
            print('available assets in the scene:')
            for act in bpy.data.actions :
                if act.import_data.asset_type != 'NONE':
                    print(act.name)
            return{'CANCELLED'}
        
        if self.root_name == 'None':
            self.root_name = asset.import_data.selected_roots.split(',')[0]

        # CHECK if root already exist in the scene and force hard import if not
        if not self.hard_import:
            root = bpy.data.objects.get(self.root_name)
            if root.is_library_indirect:
                print(' root %s not local, FORCE hard import'%self.root_name)
                prop.hard_import = True

        if asset.import_data.asset_type not in ['POSE', 'ANIM'] :
            print('CANCEL: asset %s is not a pose or an animation'%self.asset_name)
            return{'CANCELLED'}
        #check if root object exist in the linked scene
        root = bpy.data.objects.get(self.root_name)
        if not root:
            print('CANCEL: root %s not found'%self.root_name)
            return{'CANCELLED'}
        
        prop.root_enum = self.root_name
        
        prop.hard_root =  self.root_name

        
        # call gpcutout.importer operator
        bpy.ops.gpcutout.importer(selected_asset=self.asset_name)
        prop.hard_import = False
        if self.keep_linked == False:
            #remove scene
            src_scene = [s.name for s in bpy.data.scenes if s.library and self.asset_source_scene in s.library.filepath][0]
            print(f'Removing scene {src_scene}')
            if src_scene in bpy.data.scenes.keys():
                print('scene found')
                bpy.data.scenes.remove(bpy.data.scenes[src_scene], do_unlink=True)
            pass
        print('DONE')
        return {'FINISHED'}

def get_action_by_name(self, name):
    for action in bpy.data.actions:
        #if it hasn't import_data attribute Pass
        if not hasattr(action, 'import_data'):
            continue
        if action.library :
            action_lib_filepath = os.path.normpath(bpy.path.abspath(action.library.filepath))
            #print(action.library.filepath, self.scene_path)
            if action_lib_filepath[2:] in self.scene_path:
                if action.name == name:
                    actionAsset = action
                    break
    return actionAsset          

class GPCO_OT_READ_ASSET(bpy.types.Operator):
    bl_idname = "gpco.read_asset"
    bl_label = "Import asset"     
    bl_options = {'REGISTER', 'UNDO'}

    keep_linked : BoolProperty(default=True)
    mc_pose : BoolProperty(default=False)
    lib_name : StringProperty()
    asset_type : StringProperty()
    is_linked : BoolProperty(default=False)
    force_draw : BoolProperty(default=False)
    scene_path : StringProperty()
    asset_source_scene : StringProperty()


    def execute(self, context): 

        prop  = context.scene.import_properties
        if prop.hard_import:
                for bool in ['flip','selected_only']:# to avoid incompaptible selection                    
                    prop[bool]=False
        self.is_drawn = False
        if not self.is_linked :
            # Link the scene 
            with bpy.data.libraries.load(self.scene_path, link=True) as (data_from, data_to):
                data_to.scenes = data_from.scenes
                print("scene ", self.asset_source_scene, " has been linked")
                self.is_linked = True
                            
        #call the right importer     
        
        import_data = context.window_manager.current_asset_action.import_data
        if (import_data.asset_type == 'MC' 
            or import_data.asset_type == 'POSE' and self.mc_pose == True):
            bpy.ops.gpcutout.mc_modal('INVOKE_DEFAULT',keep_linked = self.keep_linked)
        elif import_data.asset_type in ['POSE', 'ANIM'] :
            bpy.ops.gpcutout.importer()
            #if self.keep_linked == False : 
            #    #unlink src scene
            #    bpy.data.libraries.remove(bpy.data.libraries[self.lib_name])#TODO FIX removing the whole library will clear materials (they shouldbe appened ?)
        #print('!!!!!!!!!!!!!!!!!!!!!',self.asset_source_scene)
        with bpy.data.libraries.load(self.scene_path, link=True) as (data_from, data_to):
            src_scene_name = data_from.scenes[0]

        if not self.keep_linked and import_data.asset_type in ['POSE', 'ANIM']:
            print('Removing scene --', src_scene_name)
            bpy.data.scenes.remove(bpy.data.scenes[src_scene_name])

        print('TOTAL TIME = ',time.time() - self.start_time)
        print('----------------------------------------------END')
        
        prop['hard_import'] = False
        # prop.set_import(False)
        return {"FINISHED"}
    
    def invoke(self, context, event):
        '''
            Get the corresponding asset 

        '''
        self.start_time = time.time()
        print('--------------------GP CUT OUT----------------')
        print("read selected asset")
        area_assets_browser, area_view_3D, region_view_3D, saved_pers = get_asset_browser_area(self,context)

        # Temp override context to find the selected asset
        # get its path and source scene

        #TODO use factorized def in general_utils.py
        with context.temp_override(area=area_assets_browser):           
            selected_asset = context.asset_file_handle # May need correction in the futur DOC: 'Avoid using this, it will be replaced by a proper AssetHandle design'
            if selected_asset:
                print("Selected Asset : ",selected_asset)
                current_library_name = context.area.spaces.active.params.asset_library_ref
                try:                    
                    library_path = Path(context.preferences.filepaths.asset_libraries.get(current_library_name).path)
                except:
                    self.report({"WARNING"}, "You are in 'ALL' library, please select the right library")
                    return {"CANCELLED"}
            else:
                print("No Asset Selected")
                return {"CANCELLED"}
            # DONE Ask the user if he wants to link the scene definitively or just 
            # the time to import his asset\ template
        
        asset_fullpath = library_path / selected_asset.relative_path
        print("Asset file path : ", asset_fullpath)
        pathParts = str(asset_fullpath).split("\\")
        self.scene_path = ""
        for part in pathParts:
            self.scene_path += part+"\\"
            if part.find(".blend") != -1:
                self.asset_source_scene = part.split('.')[0]
                self.scene_path = self.scene_path[:-1]
                print("scene path : ", self.scene_path)
                print("Asset src scene : ", self.asset_source_scene)
                break
        
        source_scene = None
        # Check if the scene is already linked in the current shot
        for scene in bpy.data.scenes:
            # if scene.name == self.asset_source_scene:
            if scene.library is not None and os.path.normpath(bpy.path.abspath(scene.library.filepath))[2:] in self.scene_path:
                print(scene.name, " is already in the current scene")
                source_scene = scene
                self.is_linked = True
                break
            else:
                self.is_linked = False

        # if the scene isn't already linked link it
        self.link_scene_if_unlinked()
                
        # Get the corresponding action in the linked scene and get its import data
        actionAsset = get_action_by_name(self,selected_asset.name)
        if not actionAsset :
            print('error-------------------------------- no action asset found')
            print(selected_asset, selected_asset.name)
            print('abort')
            return{'CANCELLED'}
        
        context.window_manager.current_asset_action = actionAsset
        self.asset_type = actionAsset.import_data.asset_type
        self.lib_name = self.asset_source_scene + '.blend'

        # CONSOLE PRINT    
        print("asset_type",actionAsset.import_data.asset_type)
        print("from frame",actionAsset.import_data.range[0])
        print("to frame",actionAsset.import_data.range[1])
        print("grid",actionAsset.import_data.grid[0], "by", actionAsset.import_data.grid[1])
        
        

        #SET POSSIBLE ROOTS FOR IMPORT
        items = []
        ind = 1
        src_scene = actionAsset.import_data.src_scene
        # selected_asset_action = context.window_manager.current_asset_action
        #for ob in actionAsset.asset_root_objects:
        possible_roots_str =''
        for aro in actionAsset.asset_root_objects:
            src_objs = get_objects_and_childs([aro.object],src_scene.objects)
            for obj in src_objs : 
                # bpy.types.Scene.possible_roots.add(obj)
                #self.possible_roots_obj.append(obj)
                possible_roots_str+= obj.name+','
                # items.append(( obj.name, obj.name, "Mesh %s" % obj.name ,ind))
                ind+=1
        prop  = context.scene.import_properties
        prop.set_possible_roots(possible_roots_str)
           
        #DRAW OPTION MENU IF NEEDED
        with context.temp_override(area=area_view_3D, region= region_view_3D):  
            area_view_3D.spaces[0].region_3d.view_perspective = saved_pers
            # if not sceneIsLinked :
            #if alt is pressed
            if self.force_draw or not self.keep_linked:
                self.is_drawn = True
                wm = context.window_manager
                return wm.invoke_props_dialog(self)
            else :
                print('read asset time =', time.time() -self.start_time )
                self.execute(bpy.context)
                return{'FINISHED'}

    def link_scene_if_unlinked(self):
        if not self.is_linked :
            self.keep_linked = False
            # Link the scene 
            #if lib is already partially linked, reload it to make sur every pointers are ok
            reload_lib = any([lib.filepath[2:] in self.scene_path for lib in bpy.data.libraries])
            with bpy.data.libraries.load(self.scene_path, link=True) as (data_from, data_to):
                data_to.scenes = data_from.scenes
                print("scene ", self.asset_source_scene, " has been linked")
                self.is_linked = True
                # source_scene = bpy.data.scenes[self.asset_source_scene]
            if reload_lib :
                print('Reloading lib')
                [lib for lib in bpy.data.libraries if lib.filepath[2:] in self.scene_path][0].reload()
            
            #for scn in data_to.scenes :
            #    try:
            #        scn.library.filepath = bpy.path.relpath(scn.library.filepath)
            #    except : 
            #        print('WARN : cant set scene path to relative : ', scn.library.filepath )
            #        pass

    def draw(self, context):
        #Forbid draw if execute to avoid ACCES VIOLATION ERROR
        if self.is_drawn:
            prop  = context.scene.import_properties
            layout = self.layout
            title = 'gp_cutout version '+prop.version
            layout.label(text=title)
            layout.label(text='Asset type : '+self.asset_type)
            layout.label(text='Asset source scene : '+self.asset_source_scene)


            layout.prop(self, "keep_linked", text='keep the scene linked for future imports')
            
            
            if  self.asset_type!='NONE' or  prop.hard_import:#self.asset_type!='MC' or 
                layout.prop(prop, "hard_import", text='import asset from scratch')
            else:                
                layout.label(text='Impossible to import from scratch from a master controller')
            
            if prop.hard_import:
                # for bool in ['flip','selected_only']:# to avoid incompaptible selection                    
                #     prop[bool]=False
                layout.prop(prop, "use_current_collection", text='import in current collection')
                row = layout.row()
                row.prop(prop, 'roots_enum' , text='pick import root')
            else:
                layout.prop(prop, "selected_only", text='Import selected only')
                layout.prop(prop, 'flip', text='import fliped asset')
                if self.asset_type=='POSE':
                    layout.prop(self,'mc_pose', text='Interpolate from current pose')
                

                pass

class GPCO_OT_PREVIEW_ASSET(bpy.types.Operator) :
    bl_label = "preview an asset"
    bl_idname = "gpco.preview_asset"
    bl_options = {'REGISTER', 'UNDO'}

    path_preview: StringProperty()

    def execute(self, context):
        #open file in os
        print('opening',self.path_preview)
        os.startfile(self.path_preview)
        return {'FINISHED'}

def get_asset_browser_area(self, context):
    area_assets_browser = None
    area_view_3D = None
    if context.screen is None:
        bpy.context.view_layer.update()
    if context.screen is None:
        return None,None,None,None
    for area_iter in context.screen.areas:
        #print(area_iter.type)
        if area_iter.type == "FILE_BROWSER" and area_iter.ui_type == 'ASSETS':
            area_assets_browser = area_iter
        if area_iter.type == "VIEW_3D" :
            area_view_3D = area_iter
            for region_iter in area_view_3D.regions :
                if region_iter.type == 'WINDOW':
                    region_view_3D = region_iter
                    saved_pers = area_view_3D.spaces[0].region_3d.view_perspective
                    break
    return area_assets_browser,area_view_3D,region_view_3D,saved_pers

def create_empty_asset(self, name, asset_collection):
    asset_empty = bpy.data.objects.new(name, None)
    asset_collection.objects.link(asset_empty)
        
        # Create action on assetEmpty
    if asset_empty.animation_data is None :
            #print('create anim_data')
        asset_empty.animation_data_create()

    if asset_empty.animation_data.action is None :
            #print('create Action')
        asset_empty.animation_data.action = bpy.data.actions.new(name = asset_empty.name)
    return asset_empty

def get_asset_collection(self, context):
    asset_collection = None
    for col in context.scene.collection.children_recursive:
        if col.name.startswith('Asset_emptys_Collection') :
            asset_collection = col
            break
    if asset_collection is None :
        asset_collection = bpy.data.collections.new('Asset_emptys_Collection')
        context.scene.collection.children.link(asset_collection)
        asset_collection.hide_render = True
        asset_collection.hide_viewport = True
    return asset_collection

def get_asset_by_name(name):
    return bpy.data.assets.get(name)

def get_asset_data(self, context, asset_name = None):
    if  asset_name is None or asset_name == 'None':
        asset_name =  bpy.context.asset_file_handle.name
    actionAsset = bpy.data.actions.get(asset_name)
    data = actionAsset.import_data
    # context.window_manager.current_asset_action = actionAsset
    # data = context.window_manager.current_asset_action.import_data
    return data

def delete_asset(context,asset_name,log='',remove_preview = True):
    # TO DELETE ? Get collection 
    asset_collection = None

    for col in context.scene.collection.children_recursive:
        if col.name.startswith('Asset_emptys_Collection') :
            asset_collection = col
            break
        
    actionAsset = bpy.data.actions.get(asset_name)
    context.window_manager.current_asset_action = actionAsset #why?
    log+=' found \n import data:'
    import_data = actionAsset.import_data
    if import_data:
        log+=' found \n src_scene:'      
        src_scene = import_data.src_scene          
        if src_scene:
            asset_scene_name = src_scene.name  
            log += ' found \n source scene : '+ asset_scene_name +'\n'                
            if actionAsset.import_data.src_scene.name == context.scene.name:# if there is a path_preview, delete it
                if actionAsset.import_data.preview_path!='' and remove_preview:
                    log+= 'Deleting asset preview ' + actionAsset.import_data.preview_path+'/n'
                    try :
                        os.remove(actionAsset.import_data.preview_path)
                    except FileNotFoundError :
                        pass
                for o in [asset_name]:
                    log+= 'Clearing asset ' + o +" : "
                    # log+= str(bpy.ops.asset.clear(set_fake_user=False))+'\n'
                    obj = bpy.data.objects.get(o)
                    if obj:
                        # obj.select_set(True)
                        # log+= 'Unlinking asset ' + o.name +" : "
                        # log+= str(bpy.ops.outliner.id_operation(type='UNLINK'))+'\n'
                        objs = bpy.data.objects
                        log+= 'Deleting asset ' + o
                        # objs.batch_remove(o)
                        str(objs.remove(obj, do_unlink=True))+'\n'
                        action = bpy.data.actions[o]
                        log+= 'Deleting action ' + o
                        
                        bpy.data.actions.remove(action, do_unlink = True)
                        
                    else:
                        log+= 'Empty ' + o.name + ' not found in current scene \n Delete failed \n'
                        log+= 'this asset may have been created with older version of GPCO \n please delete it manually'
                    
                    
                        
                    
                #delete asset empty collection
                if len(asset_collection.objects) == 0:
                    log+= 'Deleting asset empty collection ' + asset_collection.name 
                    bpy.data.collections.remove(asset_collection)
                    
                    

    return log

def set_selected_gp_as_asset_roots(self,context):
    possible_roots_str =  ''
    selected_roots_str = ''
    for obj in context.scene.objects:
        if obj.type == 'GPENCIL': 
            #If not selected, add it to the list
            if not obj.select_get():
                possible_roots_str+= obj.name+','
            else:
                selected_roots_str+= obj.name+','


            
    data = context.scene.import_data
    data.src_scene=(context.scene)
    data.set_possible_roots(possible_roots_str)
    data.set_selected_roots(selected_roots_str)

from .common.render_utils import get_viewport_gif, GPCO_OT_GENERATE_ASSETS_GIFS, render_image_sequence

def generate_custom_preview(context, asset_empty):
    import_data = asset_empty.animation_data.action.import_data
    try:
        snapshots_path = render_image_sequence(bpy.path.abspath('//'),
                        prefix= import_data.name,
                        starting_frame=import_data.range[0],
                        ending_frame=import_data.range[0],
                        use_viewport=True)
        override = context.copy()
        # Set context "id" member to some ID, e.g. a material.
        override["id"] = asset_empty.animation_data.action
        with context.temp_override(**override):
            bpy.ops.ed.lib_id_load_custom_preview(filepath=snapshots_path[0])
    except Exception as e:
        print('error while generating custom preview:', e)
        print('fallback to auto preview') 
        #Old autopreview
        
        asset_empty.animation_data.action.asset_generate_preview()

class GPCO_OT_MARK_SUBPROCESS(bpy.types.Operator) :
    bl_label = "Create an asset in current scene with pythonic seriable variables only"
    bl_idname = "gpco.mark_subprocess"
    bl_options = {'REGISTER', 'UNDO'}

    asset_name: StringProperty()
    
    source_file_path: StringProperty()
    hard_import: BoolProperty()
    # serialized dic variable
    name: StringProperty()
    asset_type: StringProperty()
    description: StringProperty()
    root_names_string: StringProperty()
    range_start: IntProperty()
    range_end: IntProperty()
    grid_x: IntProperty()
    grid_y: IntProperty()
    preview_path: StringProperty()
    catalog_id: StringProperty()
    make_preview: BoolProperty(default=True)

    def execute(self, context):
        root_names = self.root_names_string.split(',')

        #rebuild dictionary
        stored_data = {
            'name': self.name,
            'asset_type': self.asset_type,
            'description': self.description,
            'selected_roots': self.root_names_string,
            'range': (self.range_start,self.range_end),
            'grid': (self.grid_x,self.grid_y),
            'preview_path': self.preview_path,
            'catalog_id': self.catalog_id
        }

        external_file_asset_conformation(self,context,root_names,self.source_file_path,self.asset_name,
                                         stored_data=stored_data,
                                         hard_import=self.hard_import,
                                         make_preview = self.make_preview)
        
        return {'FINISHED'}

def external_file_asset_conformation(self=None,
                                     context=bpy.context,
                                     root_names=[],
                                     current_file_path='',
                                     asset_name='',
                                     stored_data={},
                                     hard_import=False,
                                     make_preview=True
                                     ):
    # set active frame at last keyframe + 1

    timeline_position = int(find_scene_last_keyframe(context.scene)+1)
    bpy.context.scene.frame_set(timeline_position)

    # select asset roots
    if not hard_import:
        has_root = False
        for obj in bpy.context.scene.objects:
            if obj.name in root_names:
                obj.select_set(True)
                has_root = True
            else:
                obj.select_set(False)
        if not has_root:
            print('-Root object not found in external scene,need to hard import')
            hard_import = True
            

    # apply source asset to it
    # context_override = {'scene': bpy.context.scene}
    # with context.temp_override(**context_override):
    bpy.ops.gpco.command_read_asset(

        asset_source_scene = current_file_path,
        asset_name = asset_name,
        hard_import = hard_import
    )

    #add frame offset to range 
    stored_data['range'] = (timeline_position,(stored_data['range'][1]-stored_data['range'][0])+timeline_position)                    
            

    # Create asset in it
    #look for asset-empty collection
    asset_collection = get_asset_collection(None,context)            
    # create empty as asset holder
    asset_empty = create_empty_asset(None,stored_data['name'], asset_collection)             

    #conform asset
    asset_empty.name = stored_data['name']
    #rename action
    asset_empty.animation_data.action.name = stored_data['name']

    #Copy temp import data to asset action
    import_data = asset_empty.animation_data.action.import_data
    import_data.set_from_dictionary(stored_data)            
    import_data.asset_empty = asset_empty
    #Holding root_objects pointers into collectionProperty
    import_data.update_asset_roots(context)

    #HACK source_scene has to be curennt scene
    import_data.src_scene = bpy.context.scene
    

    # Mark the asset
    asset_empty.animation_data.action.asset_mark()
    asset_data = asset_empty.animation_data.action.asset_data
    asset_data.description = stored_data.get('description','')
    asset_data.catalog_id = stored_data.get('catalog_id')

    #generate custom preview
    #make preview and gif generation optionnal : not needed with clipboard pasting
    remove_preview = False
    print('####################################******************#########', make_preview)
    if make_preview :
        generate_custom_preview(context, asset_empty)

        #move gif from stored path to new path
        #get file path without file name

        new_gif_path = os.path.join(os.path.dirname(bpy.data.filepath), asset_name+'.gif')
        old_gif_path =  stored_data.get('preview_path','')
        if old_gif_path != '' and new_gif_path!=old_gif_path and os.path.exists(old_gif_path):
            remove_preview = True
            #Avoid BUG IF FILE ALREADY EXIST
            try:
                if not os.path.exists(new_gif_path):
                    # os.rename(old_gif_path, new_gif_path)
                    #avoid BUG if move across drives
                    shutil.move(old_gif_path, new_gif_path)
                else:
                    print('WARNING! Cannot move GIF as a file with same name already exist!')
            except Exception as e:
                print('ERROR while moving GIF:', e) 
                        
            import_data.preview_path = new_gif_path
        else:
            print('no gif to move')
            remove_preview = False
    
    # save  the scene
    bpy.ops.wm.save_mainfile()

    return remove_preview

class GPCO_OT_MOVE_ASSET(bpy.types.Operator, ImportHelper): 
    bl_label = "Move an asset in an external scene"
    bl_idname = "gpco.move_asset"
    filter_glob: StringProperty( default='*.blend', options={'HIDDEN'} ) 
    name : StringProperty(default = 'None')
    bl_options = {'REGISTER', 'UNDO'}
    delete_if_cancelled : BoolProperty(default = False)
    filepath : StringProperty(subtype='FILE_PATH')
    make_preview : BoolProperty(default = True)

    def invoke(self, context, event) :
        self.data  = get_asset_data(self, context, self.name)
        current_file_path = bpy.data.filepath
        
        # get current library path and if none set to current file path
        lib_path = get_library_path(context)
        if lib_path is None:
            print('WARNING : no library path found, using current file path (did you select an asset in asset browser?)')
            lib_path = str(current_file_path)
        else:
            lib_path = str(lib_path)+'\\temp.blend'

        # if  not lib_path.endswith('.blend'):
        #     lib_path = lib_path + '.blend'

        self.filepath = lib_path
        return super().invoke(context, event)

    def execute(self, context):      
        # check if self.data is an existing attribute
        if not hasattr(self, 'data'):
            self.data = get_asset_data(self, context, self.name)
        print("--- moving asset to ---)", self.filepath)
        
        stored_data = self.data.get_as_dictionary()
        asset_name = self.data.name
        root_names_string = self.data.selected_roots
        root_names = root_names_string.split(',')
        #save current blender file (same operator a ctrl.s)
        bpy.ops.wm.save_mainfile()

        # Save the current scene so we can revert back to it later
        current_file_path = bpy.data.filepath

        if  not self.filepath.endswith('.blend'):
            self.filepath = self.filepath + '.blend'

        hard_import = False
        #check if file exist
        if not os.path.exists(self.filepath):
            hard_import = True
            print('-File does not exist, need to create it')   
            
            '''#THIS CODE WORKS, safer but slower
            empty_path = os.path.join(os.path.dirname(__file__), 'demo files', 'empty_template.blend' )
            destination_path = str(self.filepath).replace('\\', '\\\\')
            args= [bpy.app.binary_path, empty_path, '-b', '--python-expr', f'import bpy \nbpy.ops.wm.save_as_mainfile(filepath="{destination_path}")']
            subprocess.run(args)'''

            #THIS CODE WORKS FASTER but assumes template.blend has no libraries
            empty_path = os.path.join(os.path.dirname(__file__), 'demo files', 'empty_template.blend' )
            destination_path = str(self.filepath).replace('\\', '\\\\')
            shutil.copy2(empty_path, destination_path)

        #turn into serializable variables
        name = stored_data['name']
        asset_type = stored_data['asset_type']
        description = stored_data['description']
        range_start = stored_data['range'][0]
        range_end = stored_data['range'][1]
        grid_x = stored_data['grid'][0]
        grid_y = stored_data['grid'][1]
        preview_path = stored_data.get('preview_path','')
        catalog_id = stored_data["catalog_id"]
        BLENDER_PATH = bpy.app.binary_path
        # Get the source code of the function
        # function_code = inspect.getsource(external_file_asset_conformation)

        #Add a call to the function at the end of the script 
        function_code =f'''import bpy

bpy.ops.gpco.mark_subprocess(
    name = "{name}",
    asset_type = "{asset_type}",
    description = "{description}",
    root_names_string = "{root_names_string}",
    range_start = {range_start},
    range_end = {range_end},
    grid_x = {grid_x},
    grid_y = {grid_y},
    preview_path = r"{preview_path}",
    catalog_id = "{catalog_id}",
    source_file_path = r"{current_file_path}",
    asset_name = "{asset_name}",
    hard_import = {hard_import},
    make_preview = {self.make_preview}

)   

            '''
            # Write the function code to a temporary file
        with open("temp_function.py", "w") as file:
            file.write(function_code)

        # Run Blender with the script
        subprocess.run([BLENDER_PATH, str(self.filepath), "-b", "-P", "temp_function.py"])

        delete_asset(context,asset_name,remove_preview=False)

        try:
            bpy.ops.asset.library_refresh()
        except:
            print('ERROR : could not refresh library')
            self.report({'INFO'}, message= 'An error orrucred, You should manually refresh the Asset browser')

        return {'FINISHED'}

    def cancel(self, context):
        print("File selection was cancelled.")
        if self.delete_if_cancelled:
            print("DELETING TEMP ASSET")
            delete_asset(context, self.name)

        return None    

class GPCO_OT_MARK_ASSET(bpy.types.Operator) :
    bl_label = "Create an asset"
    bl_idname = "gpco.mark_asset"
    bl_options = {'REGISTER', 'UNDO'}


    # asset_type : EnumProperty(items=[('NONE', 'None', 'None', 0), ('POSE', 'Pose', 'Simple Pose', 1), ('ANIM', 'Animation','Animation template',2),('MC', 'Master Controller', 'Master controller template', 3)], default = 'NONE')
    # start : IntProperty()
    # end : IntProperty()
    # grid : IntVectorProperty(size = 2,default=(1, 1), min=1)
    # name : StringProperty()
    # description : StringProperty()
    # asset_prop: PointerProperty(type=ASSET_PROPS) 
    cancel : BoolProperty(default = False)
    is_external: BoolProperty(default = False)
    make_preview : BoolProperty(default = True)


    def invoke(self, context, event) :
        print('INVOKE')
        if self.is_external:
            print('external asset creation')
        # SET IMPORT DATA
        #get all gpencil object as potencila root

        set_selected_gp_as_asset_roots(self,context)
        # get the index of the currently selected keyframe
        # if there is no keyframe, return 0

        #get the catalog file and generate the enum prop
        current_library_name = context.area.spaces.active.params.asset_library_ref
        if current_library_name == 'LOCAL':
            library_path = os.path.dirname(bpy.data.filepath)
        else :
            library_path = context.preferences.filepaths.asset_libraries.get(current_library_name).path

        catalogs = None
        if 'blender_assets.cats.txt' in os.listdir(library_path):
            #print('Found catalog file')
            catalogs = self.parse_asset_catalog(os.path.join(library_path, 'blender_assets.cats.txt'))
            #print(catalogs)
            
        else :
            #print('No catalog file found')
            pass
        self.create_enum_property_from_dict(catalogs)

        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
       #draw_asset_properties(self,context)
       draw_asset_from_import_data(self,context)

    def execute(self, context):
        if not self.cancel :
            data  = context.scene.import_data


            if verbose:''' SANITY CHECK '''
            sanity = data.sanity_check(context,
                                       update_root = False,
                                       enforce_kf = True, 
                                       update_src_name = False)

            if not sanity :
                print('error in sanity check')
                return{'CANCELLED'}
            
            
            if verbose:''' CREATE ASSET HOLDER'''
            print('+++ Creating asset holder :', data.name)
            #asset holder is an action linked to an empty object

            #look for asset-empty collection
            asset_collection = get_asset_collection(self,context)            
            # create empty as asset holder
            asset_empty = create_empty_asset(self,data.name, asset_collection)
                
            data.self_copy_to_asset_empty(context,asset_empty)

            if self.is_external:
                bpy.ops.gpco.move_asset('INVOKE_DEFAULT',name = data.name, delete_if_cancelled = True, make_preview=self.make_preview)

            print('DONE')
            return {'FINISHED'}
        else :
            return{'CANCELLED'}
        
    def parse_asset_catalog(self, file_path):
        """
        Parse a Blender Asset Catalog file and return a dictionary of catalog names and their UUIDs.
        
        :param file_path: Path to the Blender Asset Catalog file.
        :return: Dictionary with catalog names as keys and their corresponding UUIDs as values.
        """
        self.catalog_dict = {}
        
        with open(file_path, 'r') as file:
            for line in file:
                # Ignore empty lines and comments
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
        
                # Ignore the version line
                if line.startswith('VERSION'):
                    continue
                
                # Extract UUID and catalog name
                try:
                    uuid, path_name = line.split(':', 1)
                    catalog_name = path_name.split(':')[-1]  # Get the simple catalog name
                    self.catalog_dict[catalog_name] = uuid
                except ValueError:
                    print(f"Skipping malformed line: {line}")
        return self.catalog_dict

    def create_enum_property_from_dict(self, catalog_dict):
        """
        Crée une EnumProperty à partir des clés d'un dictionnaire.
        
        :param self.catalog_dict: Dictionnaire des catalogues {nom: uuid}.
        :return: EnumProperty
        """
        # Construire la liste de tuples pour l'EnumProperty
        if not catalog_dict :
            catalog_dict = {}
        items = [('None','None','')]+[(key, key, catalog_dict[key]) for key in catalog_dict.keys()]
        
        # Définir une fonction pour l'EnumProperty
        def update_catalog(self, context):
            selected_key = self.selected_asset_catalog
            #print(f"Selected Catalog: {selected_key}")
        
            # Trouver l'UUID correspondant via items
            uuid = dict([(i[0], i[2]) for i in items])[selected_key]
            #print(f"Selected Catalog UUID: {uuid}")

            #assigner l'uuid dans l'import data
            context.scene.import_data.catalog_id = uuid

        # Ajouter l'EnumProperty à une classe
        bpy.types.WindowManager.selected_asset_catalog = bpy.props.EnumProperty(
            name="Catalog",
            description="Choose a catalog",
            items=items,
            update=update_catalog,
            default='None'
        )
        

class GPCO_OT_DELETE_ASSET(bpy.types.Operator) :
    bl_label = "Delete an asset in current scene"
    bl_idname = "gpco.del_asset"
    cancel : BoolProperty(default = False)
    bl_options = {'REGISTER', 'UNDO'}

    def invoke(self, context, event) :
        wm = context.window_manager
        # https://blender.stackexchange.com/questions/73286/how-to-call-a-confirmation-dialog-box
        return wm.invoke_props_dialog(self)
    
    def draw(self, context):
        # layout = self.layout
        # col=layout.column()
        # asset = None
        pass

    def execute(self, context):
        log = ""
        # Verify Scene 
        asset = bpy.context.asset_file_handle
        

        # printing import data just to be sure
        log+='action asset:'
        if asset:
            log+=delete_asset(context,asset.name,log=log)
            return {'FINISHED'}
                    
        # row = layout.row()
        # row.label(text = "This asset isn't in this scene.\n Open source to delete.", icon = 'ERROR')
        log+="not found in current scene \n Delete operation cancelled"
        print(log)
        return{'CANCELLED'}   

class GPCO_OT_EDIT_ASSET(bpy.types.Operator) :
    bl_label = "Edit an asset"
    bl_idname = "gpco.edit_asset"
    bl_options = {'REGISTER', 'UNDO'}

    name : StringProperty()
    asset_type : EnumProperty(items=[('NONE', 'None', 'None', 0), ('POSE', 'Pose', 'Simple Pose', 1), ('ANIM', 'Animation','Animation template',2),('MC', 'Master Controller', 'Master controller template', 3)], default = 'NONE')
    start : IntProperty()
    end : IntProperty()
    grid : IntVectorProperty(size = 2)
    description : StringProperty()
    source_scene_name : StringProperty()
    

   
    # source_path: StringProperty(subtype='FILE_PATH')
    def draw(self,context):
        # draw_asset_properties(self,context)
        draw_asset_from_import_data(self,context)
    # cancel : BoolProperty(default = False)

    def execute(self, context):
        data = context.scene.import_data
        asset = bpy.context.asset_file_handle
        # actionAsset = bpy.data.actions.get(asset.name)
        empty_asset = bpy.data.objects.get(asset.name)

        if verbose:''' SANITY CHECK '''
        sanity = data.sanity_check(context,
                                update_root = False,
                                enforce_kf = True, 
                                update_src_name = False)

        if not sanity :
            print('error in sanity check')
            return{'CANCELLED'}
        
        if verbose:''' UPDATE ASSET DATA '''
        data.self_copy_to_asset_empty(context,empty_asset)

        return {'FINISHED'}

    def invoke(self, context, event):
        possible_roots_str =  ''
        selected_roots_str = ''

                
        data = context.scene.import_data
        data.src_scene=(context.scene)
        


        asset = bpy.context.asset_file_handle
        actionAsset = bpy.data.actions.get(asset.name)

        for obj in context.scene.objects:
            if obj.type == 'GPENCIL': 
                #If not selected, add it to the list
                if obj.name not in actionAsset.import_data.get_selected_roots_names():
                    possible_roots_str+= obj.name+','
                else:
                    selected_roots_str+= obj.name+','

        data.copy_asset(context, actionAsset.import_data)
        data.set_possible_roots(possible_roots_str)
        data.set_selected_roots(selected_roots_str)
        wm = context.window_manager

        return wm.invoke_props_dialog(self)

temp_dir = tempfile.gettempdir()#os.path.dirname(os.path.abspath(__file__))
CLIPBOARD_ASSET_NAME = 'clipboard_asset'
CLIPBOARD_FILE =f'\\clipboard\\{CLIPBOARD_ASSET_NAME}.blend'

class GPCO_OT_PAPERCLIP_COPY(bpy.types.Operator) :
    ''' Generate a temporary asset hold in the clipboard'''
    bl_label = "Copy asset to clipboard"
    bl_idname = "gpco.copy_asset"
    bl_options = {'REGISTER', 'UNDO'}
    current_frame = None
    
    def invoke(self, context,event):
        #set asset properties
        data  = context.scene.import_data
        data.name = CLIPBOARD_ASSET_NAME
        data.description = 'temporary asset'
        data.asset_type = 'POSE'
        #get current frame timeline
        self.current_frame = context.scene.frame_current
        data.range = (self.current_frame,self.current_frame)
        set_selected_gp_as_asset_roots(self,context)
        
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    
    def draw(self,context):
        draw_asset_from_import_data(self,context)


    def execute(self,context):
        #create asset locally
        bpy.ops.gpco.mark_asset(make_preview=False)

        abs_dir = os.path.abspath(os.path.join(temp_dir, CLIPBOARD_FILE.lstrip("\\")))
        # delete existing if exist
        # if clipboard folder doesn exist, create it
        if not os.path.exists(os.path.dirname(abs_dir)):
            os.makedirs(os.path.dirname(abs_dir))
        elif os.path.exists( abs_dir):
            os.remove( abs_dir)
            


        #move asset to paperclip external file
        bpy.ops.gpco.move_asset('EXEC_DEFAULT',
                                name = CLIPBOARD_ASSET_NAME, 
                                filepath =  abs_dir,
                                make_preview=False)
        
        #set the current frame back
        context.scene.frame_current = self.current_frame
        return {'FINISHED'}

class GPCO_OT_PAPERCLIP_PASTE(bpy.types.Operator) :
    ''' Paste the asset from the clipboard'''
    bl_label = "Paste asset from clipboard"
    bl_idname = "gpco.paste_asset"
    bl_options = {'REGISTER', 'UNDO'}

    hard_import : BoolProperty(default = False)

    def execute(self, context):
        #check if file exist
        abs_dir = os.path.abspath(os.path.join(temp_dir, CLIPBOARD_FILE.lstrip("\\")))
        if not os.path.exists( abs_dir):
            print('ERROR : no temporary clipboard file found at %s'% abs_dir)
            return{'CANCELLED'}

        bpy.ops.gpco.command_read_asset(asset_source_scene =  abs_dir,
                                        keep_linked = False,
                                        asset_name = CLIPBOARD_ASSET_NAME,
                                        hard_import = self.hard_import)
        
        #remove clipboard library
        try :
            temp_lib = [lib for lib in bpy.data.libraries if "clipboard_asset" in lib.filepath][0]
            bpy.data.libraries.remove(temp_lib)
        except : 
            pass

        return {'FINISHED'}
    
class GPCO_OT_SELECT_MASTER(bpy.types.Operator) :
    ''' Display a list of all available object so user  can choose import root'''
    bl_label = "Select a master source"
    bl_idname = "gpco.select_master"
    bl_options = {'REGISTER', 'UNDO'}
    my_pointer = PointerProperty(name="My Pointer", type=bpy.types.Object)


    def draw(self, context):
        layout = self.layout
    
        row = layout.row()
        row.prop(self, 'asset_type', text = 'Asset type ')

    def execute(self, context):
        
        asset = bpy.context.asset_file_handle
        actionAsset = bpy.data.actions.get(asset.name)
        # name = self['name']
        # for prop in ['name','asset_type','start','end','grid','description']:
        #     if actionAsset[prop]:
        #         actionAsset.import_data[prop] = self[prop]


        to_return = self.asset_type

        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

#DEPRECATED 
class GPCO_OT_CREATE_EMPTY(bpy.types.Operator) :
    ''' Create an empty scene for asset creation'''
    bl_label = "Create empty scene"
    bl_idname = "gpco.create_empty_scene"
    bl_options = {'REGISTER', 'UNDO'}

    backup_model_path: StringProperty()
    direction_path: StringProperty()

    def execute(self, context):
        create_empty_layout_scene(context= context, 
                        backup_model_path= self.backup_model_path,
                        direction_path= self.direction_path)

        return {'FINISHED'}