# TEST PROTOCOL:
    CREATE
        MC:
            ik rig V04: OK 
            noik rig: OK 
                
    IMPORT
        from scratch: append assets in an empty scene with COL-GP collection (doesn't work without!)
            Anim:
                noik rig: V04
                    from master > OK

                    from other (neck) > ok
                ik rig:(v15)  
                    from master > Ok?:
                        It seems globaly ok, but rig isn't finished yet so not sure

            MC: Not implemented yet. No need to try. option is blocked FTM
        
        on import scene: in a scene with the rig in it
            Anim:
                noik : (v04)
                    from master > ok
                    from other > ok
                ik : (v15)
                    from master > BUG: works but issues on some face elements. Probably linked to empty layers
                    from other > BUG : same issue
            MC:
                noik rig:
                    from source > ok
                            Bug:
                                -there may be an issu whit auto interpolation. 
                    from children > ok
                ik V15: BUGGY: seem to work overall but some elements are not where they are suppose to be.
        





# TO DO : 
    l'utilisation d'un MC fait sortir de la vue camera
    bug avec les data linkées : mon idée est que le sanity check à la création de l'asset renomme les dat
    a pour qu'elles aient le meme nom que les objets :  il faudrait envisager quelques éxceptions : (si la data est une librairie, si elle a plusieurs users)

# GENERAL

use new keyframe copy system for root object: while it is'nt done, if assets root object are problematic  (IK parts, hidden object...), old issues will pop back
WIP use new keyframe copy system for GP

MC import from scratch:
    DONE Forced to import from asset root no matter what root is picked in list

unlock locked fcurves on copy

DONE -il me semble avoir déjà eu ce probleme, et fait la fonction fc_is_from_array pour ça, si une fcurve n'est pas issue d'un array elle renvoie un fc.array_index à 0, mais si on veut la keyframe avec keyframe_insert, il faut donner un index de -1 

DONE MC ASSET CREATION:   keyframe enforcement seems to be creating missing keyframe but GPframe seems to be ignored by modal. This lead to weird offset between GP and Fcurve on import

-TOCHECK Probable BUG: when linking two scene, somethings off and popup seem to be poping every time na matter what. seems to be linked to scene renamed after asset creation

- DONE on edit asset, empty is not renamed, making it impossible to retrieve
- DONE avoid crach on delete on old or not conform assets
- DONE modify import in current collection so that asset root collection is replaced by current selected collection in import scene.
- DONE ce qu'il faudrait : à l'appel du modal, on est en mode clippé, en maintenant shift appuyé, on peut interpoler (du moins essayer), si on relache shift retour au clipping (modifié) 

- Missing elements hierarchie rebuild. Seems to work on Pose/Anim. Something is off with MC
- add verbose option to addon for debug
- add cutout precision (mouse magnitude) settings
- TOCHECK exlude from view layer collection property aren't passed from src to tgt (does it really need to be solved?)
- DONE orphan action on ik rig after import>> need to lookout
- scene link is keeped if abort import

- SOLVED custom, properties kframes aren't imported! je remarque que l'import de pose ne prends pas en compte les custom properties
- SOLVED reimporting materials at each call
- DONE  edit asset doesn't reapply sanity check
- SOLVED  if there is a missing element in the middle of the hierarchy (eg NECK) and you call asset_import there is a duplication of the childs already existing. They have the good name and previous asset are renamed with 001? weird.
~~Similar behaviour when importing without child.~~ not anymore

- lock:
    - if there is lock on src scene on transform fcurves or gp on hard import_ unlock on copy (ATM copy the lock)
    DONE 0.0.3 TOTEST- if there is lock on tgt scene // : avoid key import on those
DONE selected only, expected behaviour: copy only src asset on selected elements

DONE  Collection choice on import, or at least import in selected 

-importers.py l242 addon_prefs = context.preferences.addons['gp-cut-out'].preferences # BUG : addon_prefs is None when called from the asset manager?

- DONE root selection on asset creation should be a list pick. Destructive, need to wait next main merge

- synchro des attributs de clés (interpo type, keyframe type etc). keyframe_confo function created. Need to  rework import.py code to implement it

- caméra qui saute à l'import ? pas revu ce bug

- empty comme controller ?

- abort du MC à revoir (Not urgent as Ctrl+Z works well)

- CANCELED ~~simplify obj pointers so that class old them~~.( MAYBE IMPOSSIBLE odd 'already registered class' issue. Maybe some CollectionProperty limitation or maybe just registration order: CLASS > new attribute > CLASS)
                - IN REPLACEMENT create a get_parent function to keep a link to parent action. Not as robust but clean enough to keep root_objs_prop as simple as possible while ASSET_PROP handles everything

- possibility to sort assets into folder?

- DONE  improve shortct registering in _init_ using *arg or **kwarg as for the moment, alt is mandatory for second shortcut and ops porpoerties aren't passed.

- read_asset: obligé de nommer la scene comme le ficher blend 

- make mc more flexible with out of ranges (non expcetional list get and 0.0 coef when not found)


DONE  à l'import from scratch, si on a des palettes du color picker dans la scene source, il faudrait les importer en meme temps que le perso
SOLVED? à l'import from scratch, la cam de la scene de rig vient aussi dans notre scene d'anim

to improve and use: Vector calculation on GP stroke interpolation


# UPDATE LOG
call of bpy.context.view_layer.update() create access violation error

import from scratch de pose (pas encore testé depuis un mc) : soucis de PointerProperties mal pointées (vers la scene linkée plutot que vers l'objet destination 
    dans les drivers de layer transformations (pupilles)
    
    DONE dans les bone constraints pointant vers un objet exterieur à l'armature (les relation entre bones sont ok)
     -  If more than one scene is linked with asset import, import options popup no matter if keep linked is check or not


GP_IGNORE_MARK
- create an operator that place a diffrenciator marker in names, to allow multiple import distinct import
- when importing from scratch an existing root, display a warning message and rename existing with multi marker

GP_SET_AS_CURRENT
- an operator that can remove ignore mark on root and all its descendant and apply it to any non descendant that share same base name

Note: Import from sccratch not working from salim turn but do from Bjorn. Modification in keyenforcement between both rigs assets creation?
Bug 1:
 - import bjorn from scratch not current folder
 - import salim same way
 - turn salim element
 - turn bjorn element
 - repeat without bug

Bug 2:
 - import salim once
 - pose salim
 - import a second salim
 - apply pose on salim 2 >> no change on salim 1
 - apply pose on salim 1 >> no change on Salim 2
 -Apply from master >>> BUG root retunr to 0


Interpo gp cut out, ne pas interpoller les frames qui ne sont pas interpolables (clipping de la frame la plus proche). 
dans l'algo d'interpo, retravailler pour detecter les strokes succeptible de créer des artefact et clip le plus proche
- bug sur layer matrix à l'import à contourner.
Lorsqu'un gp a une matrice de transfo, a la copie, la matrice est appliquée (aux points?) + copie. il faut appliquer l'inverse de la matrice (aux points?)
- color matching sur les objets lateralizés (optionnel). rendre ça plus intelligent

TODO? on closed shape, rebuild point_list based on sapce coordinate instead of following real to avoid card flip effect on interpolation 

task1:    
     Improve banking: it takes too much time for the moment:
        - select root
        - select frame range in dopesheet
        - right click on library (we may even add a shortcut )
        - click on 'fast banking'
        - asset is pre-fill with range/asset type and just need naming and click on ok
        - add-on search for banking 'additional_poses.blend' in bank folder. bank_folder = current library folder?
        (Optional:mays be easier to duplicate rig manually and rename it)
            - if it doesnt exist:
                - creates it with necessary rig
        - else:
            charges 
        - set timing to last key + 1
        - add necessary animation to rig
        - create the asset in bank file with offset
        - save bank file 


        

    Possible bug: impossible to bank 2 different rig from the same file

    Pour la bank anim, j'ai remarqué qu'en essayant d'ajouter une "root" lors de la création de l'asset, j'ai une fenetre énorme qui s'ouvre et je dois chercher à l'oeil l'asset voulu. Si à l'avenir on pouvait avoir, soit une case "recherche", soit la possibilité de pointer sur le viewport l'asset cherché, ce serait parfait.Il serait aussi intéressant de pouvoir scripter la création de catalogs lors de la création d'un asset.
Pour l'instant, lorsque l'on crée un asset dans un external file, il se range dans le catalog "All" de la library choisie. Si on souhaite le changer de place ensuite, il faut ouvrir le fichier dans lequel l'asset a été créé pour le déplacer. C'est vraiment pas pratique.

# LOG
X |||||

ADD a catalog field at asset creation

BUG when trying to bank an animation from a second instance from a rig prefixed by ('.'). 2nd instance, created and not the right pose. Similar bug with props

Presse-Papier

0.9.2.01
 DONE Use temp folder
 DONE solve crash with files gathered with EWILAN PIPELINE TOOL

0.9.2
    DONE add PaperClip functionnality:
        PC_COPY: 
            DONE create a temporary asset using currently selected roots and kf, in animation mode
        PC_PASTE: Check for temporary asset and load it to current scene. 
            DONE use command read to bypass asset library managmeent
                - DONE soft import by defaut on existing rig
                - DONE hard_improt if asset root cannot be found on cible file
        DONE add button to call ops


    TESTS:
        With old local files:
            Copy OK
            Open Temp file OK
            Paste OK
        With new local file (gathered with EWILAN PIPELINE TOOL ):
            Copy OK
            Open temp file: Exception violation crash
            Paste: Subprocess exception violation crash

        DONE clean and factorize READ operator invoke def
    


0.9.1
    ASSET PROP
        DONE add a Research field system for root selection:
            -DONE add string property with  search function
            -DONE remove previous way
        


0.9.0 MAIN Merge

Patch 0.8.5.02

    DONE avoid having to select an asset to have the right lib path
    DONE Automatize refresh of asset browser when asset is banked. Callingasset_refresh operator
    [EXTERNAL] DONE Add template file path to package manager (experimental branch only. will need to do it on main merge too) 



Patch 0.8.5.01:
   BYPASS  material conform bug that led to commit reverse on MAIN .
    BUG is in importers.py around L904.Any access to strokes len, item, key or whatsoever can lead to EXCEPTION VIOLATION ERROR.


0.8.5:
    CREATE ASSET IN EXTERNAL
        - DONE Get inside library folder (instead of parent folder). WARNING!  Need to select an asset first or it will be at current file path
        - DONE Force .blend on external creation prompt (added if not here) creation
        - DONE correct temp_file path. For the moment it always create use backup mode if not setted manualy in ADDON properties(empty version of current file)
        - DONE add a CANCEL behaviour so that temp asset is deleted when external file  pick is canceled
        - DONE avoid FileExistsError: [WinError 183] when a gif with the same name already exist in directory
        - DONE TO CHECK avoid move across disk error OSError: [WinError 17] >> use shutils instead of os

0.8.4
DEBUG PROCESS:
    SUBPROCESS ACTIVATED
        CREATE ASSET IN EXTERNAL FILE:
            - In another folder:
                - in a new file: ASSET OK, Preview OK, GIF OK, Remove from source OK (but need ctrl+s to be saved manually. Asset dont count as changes so you wont be aske on file closure!) ,
                - in an existing file: ASSET OK, Preview OK, GIF OK, Remove from source OK (but need ctrl+s to be saved manually. Asset dont count as changes so you wont be aske on file closure!),
            - in source file folder:
                 - in a new file: ASSET OK, Preview OK, GIF OK (No gif to move triggered), Remove from source  OK (but need ctrl+s to be saved manually. Asset dont count as changes so you wont be aske on file closure!),
                 - in an existing file: ASSET OK, Preview OK, GIF OK (No gif to move triggered), Remove from source  OK (but need ctrl+s to be saved manually. Asset dont count as changes so you wont be aske on file closure!),
0.8.3
    MOVE_ASSETS:
        DONE allow use of subprocess for external file operations:
            - DONE create new move operator to use only serialisable pythonic variable
            - DONE automatically create temp.py to call from subprocess
            - DONE handle  asset move by subprocess 
            - DONE handle empty_layout file creation by subprocess
            - DONE add a use_subprocess option to choose if suprocessing needs to be used 
            - DONE add an empty_layout.blend to addon

    WARNING: There is no way to check if subprocess operation was successfull, so if subprocess use is 'on' asset may be deleted even if process failed

0.8.2
    MOVE_ASSETS:
        - DONE HANDLE UNEXISTING CIBLE FILE OR RIG:
            - DONE create file if it doesnt exists
            - DONE add an empty_scene_model_path variable to force a load model
            - DONE add empty file layout path to addon properties
            - DONE if there isn't, use current file has model. WARNING: this would result in a scene without camera. Preview will be empty!
    UI ASSET LIB:
        -DONE REWORK OPTION DISPLAYED as follow
            if lib is 'local':
                create in current, 
                if selected asset and in current:
                    edit asset,
                    delete asset,
                    export asset
            else:
                create in external file.
                if selected asset and not in current:
                    import asset

    BUGS:
        - SOLVED frame offset beetween assets and keyframe. Created asset is placed at strating frame + initial asset range
        - SOLVED Scene 'NONE' error on some asset import 
        - SOLVED if the folder where the asset is moved is the same as origin folder, gif will be deleted with asset remove


0.8.1.01:
    - DONE Delete asset by name only
    - DONE Delete asset automatically on MOVE_ASSET call

0.8.1:
    - EXTRERNAL ASSET MARK PROCESS:
        - DONE create internal asset as usual
        - DONE call MOVE_ASSET

    - MOVE_ASSET
        - DONE Avoid move option disply if no asset is selected
        - DONE ask USER for destination path
        - DONE save the current file
        - DONE open the destination file
        - DONE link the current scene in destination file
        - DONE apply the asset to the last frame:
            - SOLVED BUG common_function.py L103 context.selected_objects throw an error. replaced by a more context safe method
            - NOT RELEVANT BUG access violation on conform_anim. context overide doesnt fix it.  anim_utils/get_transformation_matrix_at_frame/set_frame create the issue. The issu seems to  the scene i'm trying to create the asset from. If i link this scene to a blank blender scene and try to acces it, blender crash too

        -DONE create the asset:
            - DONE get rid af any call to cutom property 'import_data' to avoir ACCESS VIOLATION crash
            - DONE recreate preview
            - DONE move GIF
        - DONE save destination file
        - DONE close it
        - DONE reopen the current file
        - POSTPONE remove the moved asset from source scene. Issues with selecting the asset on opening


    - SOLVED BUG cannot delete a moved asset (issue with source_scene)




O.8 Main Merge

0.7.11:
    SOLVED BUG on MC MATERIAL MATCH validation:
        Importer.py l.369-370.


0.7.10:
    MATERIAL MATCH during MC:
        - DONE create a material_table_dic that hold objects material_table
        - DONE force stroke material index update during interpolation
        - BUG Sometimes, materials aren't reasign NOT SOLVED because it is linked to update_gp_frame_display() that do cancel material index reasignation when called. It could be solved by muting update (L955 importers.py), but this would create artefacts that may be worse than swaped material 

0.7.9:
    MATERIAL MATCH
        ROOLBACK to stroke level assignement 

0.7.8:
    Material Match Rework to make it works during MC interpolation:
     SOLUTION: change paradigm by placing conformation at object level and reassign object material slot to match source material:
        -DONE create def conform_material_table
        -DONE replace method in anim_utils/conform_animation
        -DONE replace method in GPCUTOUT_OT_MCMODAL/Invoke
        -SOLVE BUG None material in slot
        -SOLVE Handle extra material in tgt by moving them in extra slots


0.7.7:
    ADD custom preview on asset creation:
        - DONE add png generation from frame 1 off asset
        - DONE overide context to current asset and call custom preview operator 

        - TO CHECK: is there a need to modify rendering mode to have alpha

0.7.6:
    BUG reporduction 
        - Create an animation asset with a root  animated and scaled
        - Import it in a new scene

    -DONE Stop creating frame on root object when it isn't frame 1 of import and there is no KF on source 
    -SOLVED when getting transform matrice, frame isn't changed on the right scene
    -SOLVED by forcing external scene view_layer update BUG root matrix apply is off. On get_transformation_matrix_at_frame def, the return matrix is always based on the frame the scene was saved on. Frame change on scene do not affect src_object transform matrix.

    -SOLVED by calling frame_set once before conformation -  BLENDER BUG to BYPASS: If external scene isn't save on first frame of anim_asset, first imports doesnt work as expected. Need to import and ctrl+z before it start behave normally. First call of frame_set(frame) on external scene has no effect



0.7.5:
    EXTEND colormatch to MC interpolation:
        - DONE Apply colormatch at invoke (but only work for first frame)
        - DONE Apply colormatch at validation
        - POSTPONE apply Colormatch at interpolation (need invasive rework of interpolate def because there isn't any access of source material in def )
        

0.7.4:
    RESTORE and stabilize colormatch:
        - Anim_utils/conform_animation
           - KEEP Old colormatch muted at L270 
           - DONE build material table at object level
           - DONE check correspondance at stroke level

    ROLLBACK common function L203: prefix is not  handled when checking which objects are missing. Construction isn't working properly on multiple instances. If kept, this will create new issue.

0.7.3:
    REWORK collection conformation:
        - DONE Conform collection even when hard_import is off ( will restore old  bug with multi collection bind on import and rebuild)
        - SOLVED BUG when a rig is rebuild (because an element is missing or it is a new instance), the whole rig is bind to root scene collection :
            - DONE make sure collection aren't created twice
            - DONE correct action 'EXT_LINK' linking to 'current_collection' collections when they are already linked somewhere.
            WARNING: This do assume that there isn't any collection linked twice in outliner 
        - USELESS Get rid of implicit multiple instance rig if neecessary
        - If not, there may be some new bug on multiple instances of the same rig:
            - SOLVED common function L203: prefix is not  handled when checking which objects are missing. Construction isn't working properly on multiple instances



0.7.2:
    BUG - pupills controller break. Impossible to reproduce the issue on work branch
    BUG - pupills controller offset. Impossible to reproduce the issue on work branch >>> probably caused by commit on experimental branch

    SOLVED -  Key error on conform_object GP_DRIVER conformation>>> Add type check

    SOLVED BUG - blush interpolation on BJORN >> add point strength calculation and conformation for strength related materials
    
    RIG ISSUE BUG - on Bjorn Mouth interpolation. Bjorn moustache shadow and mouth stroke are both on the same layer, with two different materials: This do cause materials to be interverted when one stroke disapear. Rig shouldn't use two different material on the same layer

    POSTPONE Allow reconstruction of rig, even after import from scratch.
    Because of implicit multiple import option and possibility to import in current collection, this would imply a rework of conform collection.

    
0.7.1
    Rework gp strokes interpolation:
        DONE Change paradigme:
            DONE - factorize stroke interpolation to ease algorythm modifications
            DONE - calculate resulting stroke as pseudostroke:
                DONE - ponderate mean to define stroke point number
                DONE - ponderate coord for each points
                DONE - ponderate pressure
            DONE - conform gp to pseudostroke
        
            SOLVED ISSUE: there is no way for the interpolation fonction to create a stroke on interpolation mode
                DONE - factorize to interpolate on frame level
                DONE - rebuild gp stroke that could be interpolate but do not exist
                DONE - remove gp stroke that do exist but shouldn't
                DONE - conform stroke material and other attributes when created. WARNING: assume that there is only one material used by layers (as the whole interpolation system do)
    
        DONE when one of the interpolation model GP does not have a point at index:
            - search for previous point in stroke
            - if there isn't any, normalize remaining
        DONE when there isn't any cible point or existing point < to min wheight threshold:
            - remove gp point

Merge to main 0.7

0.6.3
    WORKAROUND BUG BLENDER when there is a transform matrice on a GP layer, and this layer is duplicated (maj+D)(or copied by code), the matrice is both applied on the layers points and kept on the layer
        DONE add def undo_gp_layer_matrix_transform(gp_object) to gp_utils
        DONE call it in conform object on 'CREATE' l290

    SOLVED On pose import, root objet is set back to source scene matrix.
        changes on Anim_utils/conform_animation 'ROOT_MATRIX_SETUP' action




0.6.2
    SOLVED BUG 
        0 - import from scratch  a rig
        1 - use a MC on this rig
        2 - import from scratch a second rig
        3 - use a MC on this secon rig
        4 - Try to use a MC on first rig >>> index error
        WorkAround: import a pose on first rig before reusing MC avoid the bug for good
        Changes>>> Making the error non blocking seems to be enough
    SOLVED If 'import to current collection' is ON then object are linked to current collection even if they are already linked.
        Changes>> Collection are not conformed anymore if 'import from scratch' isn't ON. 
        WARNING! this do mean that if a collection is moved by accident,the add-on will no longer correct it.

0.6.1
    SOLVED Cannot import multiple rigs whthout addon getting confused
        Changes>>> l371 Asset Action is only searched by it's name, resulting into getting the first action with the name instead of scene action.
    SOLVED when importing multiple instances of same rig without 'import in current collection', addon create multiple instances of root collection with prefix.
        Changes in General_utils/conform_collections 


patch:

0.5.1.04:
    SOLVED Handle Offline relative path to avoid menu to popup while asset's scene is already loaded
    SOLVED call stroke.points.update() on KF enforcement
    
0.5.1.03:
    SOLVED On MC import, root object without keyfrales are taking source kf value instead of keeping initial as expected

0.5.1.02:
    SOLVED linked source scene IK_PARTS pointers are somehow overwrited by conformations while it shouldn't be possible.

0.5.1.01: 
    SOLVED: some pointers are None after conformation. Caused by prefix added multiple times when conform called multiple times on pointers. Check for prefix necessity on pointer conformation 

0.5.1:
    - Allow import of multiple instance of the same rig, with automatic naming distinction
        - DONE Import from scratch
        - DONE Pose import
        - DONE MC import
        - DONE Custom Data pointerProperty 
        - DONE Drivers pointerProperties
        - WIP Switch IK/FK  toggle and support (may imply ewilan rig addon modifications)

0.5.0: Main merge

0.4.7:
 - SOLVED Lim-loc constraint 'space_object' dir is not pointing to local obj

0.4.6:
 - DONE Add a 'NON_NATIVE_DATA' conform method and a function able to recursively conform  all the element of a non-native object prop and make its pointed object local (
    Notes: 
     - It Handles CollectionProperty, PropertyGrop and PropertyArray
     - custom data dir have to be added to a 'non_native_gp_data' list to be handled

 - DONE Use the same function to conform non-native object prop converted to custom properties due to missing add-on

0.4.5:
 - DONE Add a general gp_points_update function to remove gp points aberations
 - DONE Get rid scene name dependency for 'keep linked' option

0.4.4:
 - SOLVED crash if 'keep-linked' isn't check when importing modal assets  
   
0.4.3:
 - DONE Correct gif path when asset not local and not in direct lib path

0.4.2
 - DONE add gif path display when not found
 - DONE cant't create asset until an other asset is selected 
SOLVED but too hardcoded. custom ik_parts and rik_root properties pointing to external objects.
 - DONE On flip:  add exclusion of GP with HLP and FULLSIGHT + add layer exception with HELPERS pattern

    
0.4.1:

   COMMAND_READ_ASSET:
    '''An operator to import an asset from a command line:
    bpy.ops.gpco.command_read_asset(
        asset_source_scene = r'C:\Path\To\File\File.blend', 
        asset_name = 'The_Asset_Name',
        root_name = 'The_Root_Object_Name'(optional)
        )
    args : asset_source_scene = the absolute path to the source scene cannot be None
        asset_name = the name of the asset to import cannot be None
        root_name = the name of the root object to import from. If None, the first root object of the asset will be used
        '''
    DONE remove dependence from importers to asset window. but may need global cleanup at some point. 
    DONE create operator

------------merge 0.4.0---------------

patch 06
    DONE add import_utils and add ensure library for PIL/pillow 
patch 05 
    DONE Add GP data drivers conformation (for pupills for exemple)
    DONE handle data_path+array_index drivers conformation
    DONE handle drivers variables targets attributes conformation 
    DONE display interpolate and flip shortcut during modal


patch 04
    DONE Redefine target of pose bone constraints to point local object

Patch 03:
    SOLVED visibility driver not linked (ctlr ik)
    SOLVED edit asset change name bug: action name not set
    SOLVED GIF GENERATION: black screen caused by default reder layer being 'composite' instead of VLYR. Compositing node activated
    RESTORED sync_palettes(src_scene, context.scene). Only triggered on hard_import to reduce view_layer update calls
    DONE- Add a shortcut to addon panel for flip operator: flip is not an operator but an option during modal, like interpolate with touch 'CTRL', and cannot be set with blender keymap management so it need to be set with event.type string value -capslock letter : 'M', full touch name : 'LEFT_SHIFT' ect


Patch 02:
    TEMPORY SOLVED by bypassing palette reload. Need to restore it. BUG EAV-crash because of bpy.ops.scene.import_palette('EXEC_DEFAULT',filepath=p)

    DONE add multi-target DriverVariable conformation
    DONE Check if driverVariable id_type are similar on conformation
    DONE Allow edit and create asset from current file library panel
    REMOVED reload_scene(bpy.data.scenes[actionAsset.import_data.src_scene.name])>>>old function that may a have been solbing a bug in the past but creating Memory access crash
    
    SOLVED BUG  KeyError: '["FREE"]' on flip . Driver Variable name issue "['FREE']" instead of 'FREE'. Catch FTM but may need closer look
    SOLVED BUG flip MC UnboundLocalError: local variable 'update' referenced before assignment



Patch 01: remove armature blocking error.
        NEW BUG 'keep linked' option is now mandatory for MC import. Blender crash if not activated

v0.3.5:
    MC from POSE
        DONE if asset is pose, add an option ''Interpolate from current pose''
        DONE generate a cible dic of two poses: cible pose and current pose and interpolate them with MC MODAL.
        DONE reverse Interpolation mode: Interpolate by default, if hold maj get back to clip
        DONE enforce KF creation if interpolating from a frame without kf and gp_kf

        SOLVED impossible to interpolate back to current. Somehow src pseudo keyframe seems to be overwrited during interpolation while pseudo_keyframe should avoid this. (link between vector kept.
        Modified keyframe_confo() and gp_keyframe_confo() but may still keep some tiny link)
        SOLVED BUG GP artefact. Link to previous bug        
        SOLVED BUG no interpolation on some elements (e.g. cheek)
        SOLVED BUG if there is no keyframe on current pose, will raise index error (searching for keyframes not in the list). Added an enforce_kf on current frame
        SOLVED BUG force_clip=True when cursor in the middle of the screen


        UNSOLVED SLOW. May be improved a tiny bit by improving gp strokes interpolation method and by hash comparaison, but using pseudo_objetcs will never be as fast as using Blenders objects.
        


v0.3.4:
    PREVIEW:
        DONE force pillow installation with pip on addon launch if not found
        DONE create a render_utils.py common for render function
        DONE create a gif renderer
        DONE use renderer to assemble gif when asset is created or edited
        DONE delete asset preview on asset delete
        DONE Add a preview operator to open gif preview

    SOLVED potential BUG If asset is edited but not saved, gif is created and kept and vice versa. so if an asset is deleted but delete isn't savec, gif is deleted. Need to check if gif is created before suggesting preview.

v0.3.3:
        DONE Add a function to automatically import gp color picker palettes from source scene
        DONE Proper operation cancelation on abort
        SOLVED? rig camera imported with asset. Cannot recreate the issue but may have been linked to bug solvedby previous patch
        SOLVED  L573 context.region can somehow be None. Need to catch
        SOLVED - Array iterator out of range during modal

TEST asset creation from animation
    MAJOR BUG pose/animation import issue when imported from a "secondary" scene: 
        SOLVED Pose>whole animation imported 
        SOLVED  Both> fantom global and master object imported. constraints skeletton duplicates. reimporting anithing leads to attaching animation to the second skelleton. Linked scene doesnt works neither. The issue is probably in prepare_to_import as animation_data aren't erased
    MINOR BUG:
        Access violation exception on mc import from scratch with keep link disabled?

Patch 01:
        SOLVED MAJOR BUGS on collection confo: Linked scene root collection is linked to current scene ifit doesn exists in scene

v0.3.2:
    MC IMPORTER:
        DONE Add a flip animation shortcut on MC. If pressed during modal, lateralized objets keyframe are replaced by  flipped values.
        DONE replace tuple system for storing animations data by dictionnary 
        WIP global factorization
        DONE add a RE pattern dictionnary to forbid GP modal interpolation on some items (CTRL, GLOBAL...)       
        SOLVED on interpolation some GP are ignored. Last_clip storage issue?
        SOLVED performance issue on interpolation

   

V 0.3.1
    POSE/ANIM IMPORTER:
        DONE factorize and simplify root object matrix calculation by putting it into anim_utils>conform_animation
        DISABLED FTM gp_utils>colormatch access violation error
        DONE Add Flipped animation import on Pose/Anim import
        DONE avoid animation confo on unlateralized object when flip is activated


  
    COMMON
        DONE update common from rigtools improvements:
            DONE anim_utils
            DONE regex_utils
            DONE general_utils :
                CHECKED conform collection behaviour as changes where destructives


-----MERGED TO MAIN: V0.3.0

patch 02:
    Root list is off on edit
    
patch 01:
    DONE clean and simplify  MC interpolate function
    SOLED issue On 2D grid MC with more than 2 rows there seems to be an issue with grid generation.    
        On 3 line grid some pose from
        On 4 line and more: out of range exception
    
V0.2.1
    SOLVED Driver delta_scale copy failed on GP_SALIM_NOSE_C, error 'list' object has no attribute 'driver' 
    REMOVE sanity check datablock renaming on asset creation
    SOLVED broken link in source scene on asset creation
    ~~CANCELED keep GPSH link to controller scene source~~
    SOLVED controller not imported on MC import from scratch
    UPDATE def sculpted_frame 
  

-----Merge To Main----- V 0.2
    

Patch 0.1.5.02:
    -DONE interpolation conform:
        DONE on Anim and pose copy source 
        DONE on MC force 'CONSTANT'

Patch 0.1.5.01: 
    -SOLVED leave camera view on import
    
v0.1.5:
    - Anim & Pose import
        DONE add a new explicit keyframe conformation system
        DONE use it for non root object to solve IK rig import issue
        POSTPONE use it for root object: while it is'nt done, if assets root object are problematic  (IK parts, hidden object...), old issues will pop back

    SOLVED after using pose and animation importer (Not an MC ) for the second time, mc method doesn't work properly.
    L446 frame_list = [kf for kf in source_fcurve.keyframe_points if self.asset_range[0] <= kf.co.x <= self.asset_range[1]] start sending weird result, like if source scene was altered at some point

patch 0.1.4.02:
    MC import from scratch:
        SOLVED Forced to import from asset root no matter what root is picked in list
        
    ANIM & POSE IMPORT 
    POSTONE to 0.1.15:
        WIP Salim head explode on pose import:
            REASON: Face collection was 'exclude from viewlayer' on collection in source file
            This seems to be hiding some transfomr to importer operator. 
            This should be avoided until solution is found. Viewlayer is a mess to handle by code
        WIP arm an legs element are imported with frame 1 transform values. 
            Need to change implicit system:
                set source_scene frame > copy transform > insert_key
            to explicit system:
                get src keyframe points > set tgt keyframe point

patch 0.1.4.01:
        SOLVED issue on action copy keeping some animation info
        RESTORED Pose asset creation range locked on 1 frame

v0.1.4 
    - SOLVED Huge version rollback to v0.1.11 on gitlab for no apparent reason. restoring from 0.1.3 backup. (mostly restored but bug solved after 0.1.11 may still pop) 
    - SOLVED import issue on fantom curves IK
    - WIP import issue on pose and anim IK. Some transform are skipped. Some constraint seems to break
    - DONE add conformation based on ik_parts for Ewylan Rig addon
    - DONE make scratch import possible from MC
    - SOLVED Orphan actions and mess with naming.

!!!!!!!!!!!version rollback to v0.1.11 on gitlab for no apparent reason!!!!!!!!!!!!!


V0.1.3:
    - SOLVED TypeError: bpy_struct.keyframe_insert() index 0 was given while property "" is not an array
    - DONE create conform function to copy gpframe without using blender copy method
    - SOLVED ASSTE sanity check: On keyframe enforcement , GPframe copy create issues when importing MC

V0.1.2:
    - SOLVED RNA removed error
    - SOLVED on edit asset, empty is not renamed, making it impossible to retrieve
    - DONE avoid error on delete on old or not conform assets
    - DONE modify import in current collection so that asset root collection is replaced by current selected collection in import scene.
    - DONE ce qu'il faudrait : à l'appel du modal, on est en mode clippé, en maintenant shift appuyé, on peut interpoler (du moins essayer), si on relache shift retour au clipping (modifié) 


patch v0.1.1.1
        SOVED 'FLIP'' object import bug

v0.1.1 - very destructive. May create new bugs ~~and non retrocompatibilty with assets created on older version~~)

        ASSET_PROPS class rework:
            DONE add sanity check function to be able to call it from edit (and maybe read) too
            - improve sanity check:
                - DONE add root objects check
            - DONE add class functions to avoid external copy/past properties and pointers to objects
            - DONE create copy_asset_data fucnction to avoid multiple variable convertion so that Edit, Mark and Read share the same data holding object type
        CREATE ASSET:
            - DONE root selection by list              
        EDIT ASSET:
            - DONE update name,thumbnail and description
            - DONE Add Sanity check
            - DONE root selection by list: Keep in mind that root list is empty on assets created before this version.
        READ ASSET
            - DONE add an import to current collection option. Less strict import that do not require the entire collection hierarchie tree to be recreated.
            -DONE Modal interpolation activated when shift is pressed instead of deactivated when ctrl is pressed
            -DONE Import custom properies keyframes
            -DONE  when there is an element missing in the hierachie avoid child duplication. 
            -WIP Rebuld missing elements in hierarchie. Seems to work on Pose/Anim. Something is off with MC: transform fcurve not imported


v0.0.6 > to 0.1.0
- DONE import selected only: works but is it the expected behaviour?
- lock option:
    ignore transformation when curve is locked
        DONE POSE/ANIM DONE but not on individual axes. e.g if any rotation axe is locked, None of the rotations will be copied 
        WIP MC
    ignore gpencil frame import when layer is locked
        POSE/ANIM/MC DONE
- DONE Improve keymap registering: register key mapping with custom import properties (possibility to add keymap in the future for option like 'selected only')

v0.0.5
- WIP: IK rig:
    - TOTEST import from scratch. Mostly working. Issue with exclude from view layers on Left legs only (maybe linked to rig behing WIP).
    - DONE MC asset creation. Seems to work well, with keyframe enforcement
    - WIP MC import
    - solved : why is there a modifier on copy while there isn't on source?
    - solved : drivers fcurve keyframes aren't copied yet. 
- DONE performance issue on move_gp_frame function
- DONE fix linked material duplication
- DONE add version display on popup to avoid confusion
- SOLVED Impossible to import from two different scenes in the same .blend
- DONE MC sanity check still using old get_childre_object function and not getting ik constraint child
- DONE MC sanity check not creating keyframes on empty gpencils layers 

v0.0.4

- DONE add secondary shortcut so that pressing alt+v display popup while not pressing it don't and keep previous settings, except if scene isn't linked already
- DONE remove asset emtpys collection when there is no remaining asset
- DONE desactivate use_autolock_layers on imported gpencils to avoid lock issues
- DONE on import from scratch, if root's collection doesn't exist in file, link it to Scene collection 

V0.0.3

- WIP: selected only option. Unexpeted behaviour. Don't try it
- DONE 0.0.3 TOTEST- if there is lock on tgt scene // : avoid key import on those
- DONE error on root determination causing unexpected behaviour on std import not starting on asset's root.
- DONE import parameter should not be accessible after first import. It make it impossible to import from scratch import from selection. Any click on properties relaunch the previous setted import
- DONE for the moment, import from scratch shoulld not be available for mc

V0.0.2
- DONE IK's driver influence aren't copied. When importing rig, every IK influences are set to 0.
- DONE drivers are destroyed on import but not recreated after. cf NECK and HEAD on hard import for eg
- DONE Some GP are ignored cause they dont have any actions


cating scene with assets in it, and removing assets from this duplicated scene, it seems that it remove source scene asset link

DONE when no asset selected, impossible to create asset   

TO IMPROVE edit asset:
    DONE avoid option display in other scene than source


BUG   File "C:\Users\j.dottor\AppData\Roaming\Blender Foundation\Blender\3.6\scripts\addons\gp-cut-out\Asset_management.py", line 296, in execute
    object.keyframe_insert(data_path = fcurve.data_path, frame = i, index = fcurve.array_index)
TypeError: bpy_struct.keyframe_insert() index 0 was given while property "["prop"]" is not an array. Propbably because of a Driver. >> voir fonction déj acodée

BUG
When initialising importation, canceling then re-importing the same asset, the choose link panel doesn't show up and link is presumed. Is link to the scene created before choice is made?

CF Hard_import initialisation (par master + collection) au layout bg ?

DIDNT SHOW FOR A WHILE : EXCEPTION VIOLATION bpy.context.scene.import_properties.hard_import = True  # Property. I can't find any call that justify this error. 
This may be related to operator properties behing continuously displayed after and during import; Maybe a draw is called.

DONE catch division by zero error l114 importers.py

DONE à la création de l'asset (donc dans l'asset browser/current file) le context menu bug avec l'erreur suivante : 

CATCH but may need more work: on a un souci d'import si par malchance on a un layer sans aucune clef (pourquoi faire ça ?) il faudra faire attention à ça en rig (plutot utiliser un empty qu'un GP vide / ajouter une clé vide par défaut) mais ce serait bien de palier à ce probleme quand même

DONE à la création de l'asset : l'auto completion du grid size est cool mais induit en erreur en effet, on peut avoir un asset de 9x1 (mc sur une ligne horizontale) mais pas encore de 1x9 (ligne verticale)


DONE - LAG : Optimisation du temps d'import GP : move_gp_frame() à revoir (remplacer par un refresh ?) DONE


WIP - layer / fcurve / object  locking

DONE  bug import anim en cas de flip sur un seul axe d'un root obj

DONE - bug au premier import (besoin d'afficher la scene linkée avant ??) DONE mais peut faire sauter la caméra


DONE - clip depth location (following prefs)

DONE - créa d'asset : bug au sanity check

DONE - index error à l'import avec la scene de rodolphe
    (c'était un souci pour les grilles de dimention x * 1 

DONE - prise en compte des contraintes et des autres fcurves
    fait pour la prépa import, et ok pour l'import pose/anim
    à terminer pour les master controllers (mettre la bonne valeur)

    - doit on garder les roots objects completement protegés ou seulement sur les transforms ? (modifiers ? custom prop ?)

DONE drivers, par exemple en cas de switch ik/fk, s'assurer que les drivers sont importés convenablement .

DONE delete asset from source scene

DONE lib all non fonctionnel> message pour dire d'aller dans la bonne librairie

DONE warn if somethings off (e.g. grid in master collection). Grid should be self checked

DONE avoid reversed animation assets?


# PERFORMANCE:
- there is no frame at frame... shoudl be checked once for a loop
- A parralel keyframe copy  system may be faster than current linear one
- 

# MAINTAINABILITY
- Need to factorize MC and POSE importers.

# EDIT tools:
    - rename on edit doesn't work
    - should take current frame as first automatically
    - should keep previous root until changed, not the one currently selected

# Hard Import >>  DONE
  import asset in an empty scene. Multiple bug to fix:
    - DONE need to check than initial colection COL-GP exist or create it
    Done creating the same collection multiple time
    - DONE  L76 commun_functions :  get_objects_and_childs not returning root object childs. Something messy with the link creation? 
    - DONE L98 importer : getting error <<root_object>> 'location' cannot be edited. 
    - DONE for the moment imported GP are invisivble. Seems to be related to previous. GP are only filled after global animation keys are copied
    - DONE BUG on pose with new rig: some animation keys of frames before and after the pose seem to be imported. May be linked to the ik system which is hidding some collections and elements keys. Precision: Some element may be constrained to hidden element . may also be link to prevuious issue
    - DONE major issue seems to be in constraitn copy. constraint keep target in source file, which  make children search result off. check name instead of object solve part of the issue but not clean solution.
# IK RIG TEST
 ## TODO
  ### MC not working  when there isn't one keyframe per grid slot on the GP
  ### RIGHT arm and leg going UP:
    On pose import: when import occure
    On 
    it may be an inversion matrix issue
  ### face explosion
  ### fOOT INVERSION  
  ### Always crash whe plugin used and try to open another .blend

 ## NOTES :
  Tests from master to master
  ### Pose:
    V13 to V13:
    working but somme with some issues ( feets reversed)

    V15 to V13:
        File "C:\Users\gerar\AppData\Roaming\Blender Foundation\Blender\3.6\scripts\addons\gp-cut-out\common\anim_utils.py", line 88, in copy_driver
        d2 = tgt.driver_add(src.data_path)
        TypeError: bpy_struct.driver_add(): property "constraints["CHD-OF_ARM_L_FOREARM_IK"].influence" not found
    
    V15 to v15: very slow + unexpected behaviour+ lot of warn : final pose is alright but a lot of unexpected interpolation to get from previous pose to this one. My guesse is that some un-keyed elements are interpolating . Hoverer Ctrl+Z seem to be working well now. warning details>> BKE_animsys_eval_driver:invalid driver - constraints["CHD-OF_ARM_L_FOREARM_IK"].influence[0]


  ### ANIM
    V13 to v13
    working but very slow and same issue with the feets
    V15 to V15 :
        -very slow . Still porcessing objects part
        -still an issue with feets . GP animation not copied. (may be a missing contraint to master but why just the feets)
        + same weird intermpolation behavious on Salim_R_arm. No warnings
        Some cloth artefacts but also present in base animation
  ### MC
    V13
        CORRECTED: Not working because of an expression issue on the rig raised by sanity check
    V15: 
        CORRECTED sanity check break'asset non conforme (grid size / range)'/ 
        - 2 crash on debugtest before this step. reason unclear
        -SOLVED with empty catch ERROR>>Importers  321 for fcurve_index, source_fcurve in enumerate(sourceObject.animation_data.action.fcurves) :AttributeError: 'NoneType' object has no attribute 'fcurves' 
            File "C:\Users\gerar\AppData\Roaming\Blender Foundation\Blender\3.6\scripts\addons\gp-cut-out\Importers.py", line 384, in invoke
        
        -TEMP added a try catch.  probably linked TODO already in comment (one frame per slot in the grid) ERROR>> frames = [gpframe_list[topleft],gpframe_list[downleft],gpframe_list[downright],gpframe_list[topright],] IndexError: list index out of range Location: c:\Users\gerar\Downloads\blender-3.6.2-windows-x64\3.6\scripts\modules\bpy\ops.py:111.
        Need to be solved quickly because unpractical FTM. For exemple an MC linked to RIG_Face wouldn't work as there is no key on root gp! 

        -still the same issue, even after rebuild a matching MC from face expressions. It seems there is no frame in gp_frameList



# COPY SYSYTEM REWORK
Block Diagram:

    -Diagnostic(asset,scene_cible) : commun a tout les imports
        - definir le point maitre :
            le point selectionné dans la cible si existe dans la source ou le point choisit de la source sinon
        - analyser toutes les différences entre l'asset source et le fichier cible en partant de ce maitre:
            -ascendante:
                -analyser uniquement les éléments necessaire au bon fonctionnement du rig depuis ce points:
                    -architecture
                    -controller de rig
            -descendante:
                - tout les elements de la collection à la gp_frame

        retourne un objet de diagnotique    
        
    - Copy(diagnotique):
        - rig --> commun a  tout les import:
            creer ou modifier tout les éléments listés par le diagnotique par ordre dépendance:
                -toutes les collections
                -tout les objets
                (...)
                -toutes les contraines 
        -animation: deux cas de figure : 
             - si pose ou anim
             - si mc



# TO CHECK:

-on salim ik turn master controller creation: Warning: Object CTLR_HAND_IK_L has no pose, unable to apply the Action before rendering

-GP_UTILS function COLORMATCH L163 : src_stroke = src_frame.strokes[i] # TOCHECK: why is src_frame.strokes not the same length as new_frame.strokes ?

-no src_gpf for  GPLYR_STROKE on some rig elements. Is it normal


Library selector Batch Library Source

Chevaux 3D





Diagnostik ik bug:
MC import from scratch: No issue
    from this each pose or anim fail somehow:
            arm and legs reversed if frame 1 not in range
            face explosed
POSE and Anim import from scratch:
    If from frame 1 no issue (except face)
    if from other frame: Break

    Hypothesis:
        linked to missing keyframe
        linked to anim importer system: somehow importer system do not attribute the right transform values on some element fcurves. e.g. GP_SALIM_ARM_L x location get frame 1 value while frame 9 is expected

        L124 src_scene.frame_set(i)
        L167 assign_t_matrix(t_matrix(sourceObject), selectedObject)
        t_matrix(sourceObject) return sourceObject first frame instead of 9th frame
        selectedObject.keyframe_insert(transform, index=-1, frame=current_frame )


