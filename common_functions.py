import bpy
import time

from .common.anim_utils import copy_driver, create_anim_data
from .common.general_utils import *
from mathutils import Vector, Matrix, Euler



is_debug = True

def t_matrix(object) :
    '''return the transformation matrix of an object as a list of 3 vectors (location, rotation, scale)'''
    return [Vector(object.location), Euler(object.rotation_euler), Vector(object.scale)]

def assign_t_matrix(t_matrix, object) :
    '''assign a transformation matrix to an object from a list of 3 vectors (location, rotation, scale)'''
    object.location = t_matrix[0]
    object.rotation_euler = t_matrix[1]
    object.scale = t_matrix[2]

def invert_t_matrix(t_matrix) : #equivalent de matrix.invert()
    '''invert a transformation matrix formed of a list of 3 vectors (location, rotation, scale)'''
    # Extraction des composantes individuelles des matrices
    loc, rot, sca = t_matrix

    # Inversion des composantes
    loc = Vector([-loc[i] for i in range(3)])
    rot = Euler([-rot[i] for i in range(3)], 'XYZ')
    sca = Vector([1 / sca[i] if sca[i] != 0 else 0 for i in range(3)])

    return [loc, rot, sca]

def combine_t_matrix(t_matrix_1, t_matrix_2, verbose=None): #equivalent de l'opération matrix1 @ matrix2 
    '''combine 2 transformation matrices formed of a list of 3 vectors (location, rotation, scale)'''

    loc = t_matrix_2[0]+t_matrix_1[0]
    loc *= t_matrix_1[2]
    loc.rotate(t_matrix_2[1])

    rot = Euler([t_matrix_1[1][index] + t_matrix_2[1][index] for index in range(3)])
    sca = t_matrix_1[2] * t_matrix_2[2]
    if verbose: print([loc, rot, sca])
    return [loc, rot, sca]


unik_prefix = '.'

def get_unik_name(name, prefix=unik_prefix, type='object'):
    '''make a name unused by adding a prefix character to the nameof object having the same name'''
    # get object with name
    match type:
        case 'object':
            obj=  bpy.context.scene.objects.get(name)
            # if object doesnt exist or is linked to external scene, return 
            if obj is None or obj.library is not None:
                return name
        case 'collection':
            col=  bpy.data.collections.get(name)
            # if object doesnt exist or is linked to external scene, return 
            if col is None or col.library is not None:
                return name
    
    return get_unik_name(prefix + name, prefix)

def get_unik_level(name,prefix=unik_prefix):
    level= 0
    while name[level] == prefix:
        level+=1
    return level

def get_unik_prefix(name,prefix=unik_prefix):
    level= get_unik_level(name,prefix)    
    return name[:level]

def get_name_at_level(name, cible_level, prefix=unik_prefix):
    src_lvl=get_unik_level(name,prefix)
    if  src_lvl == cible_level :
        return name
    else:
        offset = cible_level - src_lvl
        if offset > 0 :
            return prefix*offset + name
        else :
            return name[-offset:]


def prepare_to_import_objects(context, src_root_objects, src_scene, 
                              hard_import = False, 
                              selected_only = False, 
                              use_current_collection = True):
    '''prepare import at object level : find the root object and create missing objects or collections
    args : context = the current context (or bpy.context)
            src_root_objects = a list of root objects in the template (stored in import_data)
            src_scene = the template scene
            hard_import = if True, create all objects from scratch, even if they already exist
            selected_only = if True, only import selected objects
            use_current_collection = if True, use the current collection as the root collection for the import
    returns  : a list of root objects in the current scene, a list of all objects to process (including children and constraint children)'''
    

    #selected_objects = context.selected_objects   Replaced to avoid some contexrt issue with command access to operatators
    selected_objects = [obj for obj in context.scene.objects if obj.select_get()]         
    
    '''--------------------------DIAGNOSIS--------------------------'''
    #execution check
    time_start = time.time()

    


    #Dictionary version:
    #Get selected root objects and their children
    src_ob_child_dic = get_objects_childs_dic(src_root_objects,
                                              src_scene.objects,
                                              recursive_dic={},
                                              add_self= True #hard_import # when hard importing, need to create root too
                                              )
    

        
    # TODO: if hard import,need iterate over all object to rename any object that already exist in the scene
    prefix = ''
    if hard_import :
        need_prefix = False
        for src_ob in src_ob_child_dic.values():
            loc_ob = context.scene.objects.get(src_ob.name) 
            if loc_ob and  loc_ob.library is None:
                need_prefix = True
                break

        if need_prefix :
            for root in src_root_objects :    
                while( context.scene.objects.get(prefix+root.name) is not None
                      and context.scene.objects.get(prefix+root.name).library is None):                
                    prefix +=  unik_prefix
                        
                


    
    selected_ob_childs_dic = get_objects_childs_dic(selected_objects, context.scene.objects,recursive_dic={})
    # BUGGY: create artefact
    # if not selected_only :
    #     selected_ob_childs_dic = get_objects_childs_dic(selected_objects, context.scene.objects,recursive_dic={})
    # else:
    #     selected_ob_childs_dic = {ob.name:ob for ob in selected_objects}

    print('get_objects_and_childs dic: ', time.time() - time_start)
    time_start = time.time()

    if not hard_import :
        if len(selected_objects)==0:
            print('no object selected')
            return None, None
        prefix = get_unik_prefix(selected_objects[0].name)
        level = get_unik_level(selected_objects[0].name)

        # DIC VERSION WIP
        tgt_root_objects = []
        #add the selected object that can be found in src_obj_child_dic
        tgt_root_objects += [ c for c in selected_objects if c.name[level:] in src_ob_child_dic.keys()] 
        # add obj that 

        # tgt_root_objects_set = selected_ob_childs_dic.keys() & src_ob_child_dic.keys()
        #prefix adaptation
        tgt_root_objects_set = [k for k in selected_ob_childs_dic.keys() if k[level:] in src_ob_child_dic.keys()]

        # #append those set objects into tgt_root_objects if they arent aklready in it
        tgt_root_objects += [ selected_ob_childs_dic.get(prefix+c.name) for c in src_root_objects if (c is not None
                                                              and (prefix+c.name) in tgt_root_objects_set
                                                              and c not in tgt_root_objects)
                                                              ]
        # TODO get roots as dic to avoid this slow recusive 
        
        # for k in tgt_root_objects_set :
        #     for root in src_root_objects :
        #         if root.name == k :
        #             tgt_root_objects.append(selected_ob_childs_dic.get(k))
        
        if len( tgt_root_objects) == 0 :
            print('template does not match with selected objects')          
            # throw warning message
            
                  
            return None, None



    # Dic version
    rebuild_dic = True
 
    if rebuild_dic and not hard_import:
        src_ob_child_dic = get_objects_childs_dic(tgt_root_objects, src_scene.objects, add_self=False, recursive_dic={},
                                                  #tgt_prefix = prefix
                                                )

    #Determining the missing objects    
    missing_src_ob_keys = src_ob_child_dic.keys() - selected_ob_childs_dic.keys()
    if len(missing_src_ob_keys) > 0 :
        print('------------object ', missing_src_ob_keys, 'missing in the child tree')
        #check if missing objects are in the tgt scene
        existing_ob = [k for k in missing_src_ob_keys if k in context.scene.objects.keys()]
        print('------------object ', existing_ob, 'already in the scene-----> avoidcreating them')
        selected_ob_childs_dic|= {k:context.scene.objects.get(k) for k in existing_ob}
    else:
        print('------------no missing objects')

    print('missing object: Dic ', time.time() - time_start)
    time_start = time.time()

    '''--------------------------CONFO--------------------------'''
    function_list = []
            #(function,kawrgs**) 
    # if hard_import :
    function_list.append((conform_collections,{})), # Conform collections tree
    function_list.append((conform_object,{'actions' : ['CREATE',
                                          'LINK',
                                          #'NEW_DATA', 
                                          'NON_NATIVE_DATA',                                         
                                          'PROPERTIES',
                                          
                                        #   'IK_PARTS',
                                          'PARENT',
                                          'ACTIONS',
                                          'CONSTRAINTS',
                                          
                                          'MODIFIERS',
                                          'GP_DATA',
                                          'GP_MODIFIERS',
                                          'SHADERS',
                                          'OBJ_DRIVERS',
                                          'GP_DRIVERS',
                                          'BONES_CONSTRAINTS',
                                          
                                          # linked to addon Ewilans Quest Rig Setup n Control
                                           ]})),#conform objects existence            
                                        

        
    #Dictionary version
    external_object_conformation(src_ob_child_dic, selected_ob_childs_dic,
                                 #KWARGS
                                 use_current_collection = use_current_collection,
                                 prefix = prefix,  
                                 function_list = function_list,                          
                                )
    

    '''--------------------------RESULT--------------------------'''    
    if hard_import : 
        if prefix == '' :  
            sync_palettes(src_scene, context.scene)      
        # Need to build tgt_root_objects        
        tgt_root_objects = []
        for src_ob in src_root_objects :
            if prefix+ src_ob.name not in [obj.name for obj in tgt_root_objects] :
                root = context.scene.objects.get( prefix+ src_ob.name)
                if root:
                    tgt_root_objects.append(root)

    if selected_only:
        objects_to_process = selected_objects
    else:
        objects_to_process =list( get_objects_childs_dic( tgt_root_objects, context.scene.objects, add_self=True, recursive_dic={}).values())

    # 

    return tgt_root_objects, objects_to_process
