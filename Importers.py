import math
import bpy
from bpy.props import BoolProperty, IntProperty, FloatProperty, StringProperty, IntVectorProperty, PointerProperty
import blf
from .common.anim_utils import apply_flip_transformation, conform_animation, create_anim_data, enforce_keyframe, fc_is_from_array, flip_fcurve_kf, flip_layer_frame, get_animation_kf_dic, get_fc, get_fc_array_index, interpolate_co_y, interpolate_gp_strokes, object_has_transform_keyframe,get_now_kf, old_interpolate_gp_strokes, recursive_merge_dict, vector_stroke_interpolation_calculation
from .common.gp_utils import colormatch, colormatch_dic, colormatch_from_table, conform_material_table, frame_confo, get_gplyr_from_name, get_interpolate_grid_value, get_now_gpframe, layer_interpolation, move_gp_frame, stroke_interpolation, sync_layers, sync_materials, update_gp_frame_display, update_gp_points
from .common_functions import *
from mathutils import Vector, Matrix, Euler
import numpy as np
import time
import copy


verbose = False
skip_locked = False

# check if selected object transform is locked
def get_lock(tgt_obj, transform):
    '''check if selected object transform is locked
    Args:
        tgt_obj (bpy.types.Object): object to check
        transform (str): transform to check (location, rotation_euler, scale)
        Returns:
            bool: True if transform is locked'''
    
    lock_array = []
    match transform:
        case 'location':
            lock_array = tgt_obj.lock_location
        case 'rotation_euler':
            lock_array = tgt_obj.lock_rotation
        case 'scale':
            lock_array = tgt_obj.lock_scale
        case _:
            print('transform not found %s' % transform)
    if any(lock_array) == True :
        return True
    return False

class GPCUTOUT_OT_IMPORT(bpy.types.Operator):
    """Import pose or animation from template scene"""
    bl_idname = "gpcutout.importer"
    bl_label = "Import pose or animation on selected objects"
    bl_options = {'REGISTER', 'UNDO'}
    selected_asset :StringProperty(
        name="Asset",
        description="Asset to import",
        default="",
        )
    
    def execute(self, context ):
        #get import properties
        prop = context.scene.import_properties

        self.start_time = time.time()
        print('----start importing pose/anim asset----')

        init_frame_current = context.scene.frame_current
        if self.selected_asset == '' :
            selected_asset = context.window_manager.current_asset_action
            prop.hard_root = prop.roots_enum
        else: 
            #selected_asset = bpy.data.actions.get(self.selected_asset)    
            selected_asset = None
            for action in bpy.data.actions:
                #if it hasn't import_data attribute Pass
                if not hasattr(action, 'import_data'):
                    continue
                if action.library and action.library.filepath == prop.src_scene :
                    if action.name == self.selected_asset:
                        selected_asset = action
                        break
        
            if not selected_asset:
                print('CANCEL: asset %s not found'%self.selected_asset)
                return {'CANCELLED'}

        
        src_scene = selected_asset.import_data.src_scene
        asset_range = tuple(selected_asset.import_data.range)

        if selected_asset.import_data.asset_type == 'POSE' :
            src_range = tuple((asset_range[0], asset_range[0]+1))
        else : #ANIM
            src_range = tuple((asset_range[0], asset_range[1]+1))
            
        if not prop.hard_import:                
            print('start soft import')    

            #get root objects (top parents)
            src_root_objects = []
            for src_root_obj in [ob.object for ob in selected_asset.asset_root_objects if ob.object is not None] :
                src_root_objects.append(src_root_obj)
        else:
            print('start hard import')
            # get the object with name prop.possible_roots
            src_root_objects = []
            for ob in src_scene.objects:
                if ob.name == prop.hard_root: #prop.roots_enum:
                    src_root_objects = [ob]
                    break

        print('prepare to import :', selected_asset.name, time.time() - self.start_time)
        # if not prop.hard_import:
        #     prop.use_current_collection = False

        root_objects, objects_to_process = prepare_to_import_objects(context, 
                                                                        src_root_objects, 
                                                                        src_scene,
                                                                        hard_import=prop.hard_import,
                                                                        selected_only = prop.selected_only,
                                                                        use_current_collection=prop.use_current_collection,
                                                                        )
        print('listing objects to process', time.time() - self.start_time)
        if root_objects is None or objects_to_process is None or len(objects_to_process) == 0 :
            # bpy.ops.gpco.select_master('INVOKE_DEFAULT')
            self.report({"WARNING"}, "Import: No object to process")
            return {'CANCELLED'}
    
        # # launch prepare_to_import_objects                
        # root_objects, objects_to_process = prepare_to_import_objects(context, src_root_objects, src_scene,
        #                                                                 hard_import=True)
        # pass


        #PROCESSING OBJECTS
        for tgt_obj in objects_to_process :
                self.start_time = time.time()

                
                if not prop.flip:
                    src_obj_name = tgt_obj.name
                    
                else:
                    lat_index = regex_get_lateral_index(tgt_obj.name) 
                    if lat_index is None:
                        continue 
                    src_obj_name = regex_flip_name(tgt_obj.name)#Warning: use default regex dictionnary,; keep it up to date
                
                #prefix handle
                offset = get_unik_level(src_obj_name)   
                src_obj_name = src_obj_name[offset:] 

                src_obj = src_scene.objects.get(src_obj_name) 
                # except: #no corresponding object in the template
                #     print('object ' + tgt_obj.name + ' not found')
                #     if tgt_obj.type == 'GPENCIL' :
                #         #add blank keyframe in every layer 
                #         for layer in tgt_obj.data.layers :
                #             layer.frames.new(context.scene.frame_current)
                
                if (src_obj is not None 
                    and src_obj.animation_data 
                    #and src_obj.animation_data.action is not None # this is excluding some GP with keys but no actions (NECK_IK eg)
                    ) :  
                    mid_time = time.time()
                    if tgt_obj.animation_data is None :
                        print('Warning : no animation data on object %s, creating some empty' % tgt_obj.name)
                        create_anim_data(tgt_obj)
                    
                    fcurves_to_ignore = []
                    
                    if selected_asset.import_data.asset_type in ['ANIM',"POSE"] and tgt_obj in root_objects :
                        actions = ['ROOT_MATRIX_SETUP','FCURVES_COPY','GPENCIL_FRAMES_COPY']
                    else:
                        actions = ['FCURVES_COPY','GPENCIL_FRAMES_COPY']
                    
                    ### NEW ANIM CONFORMING
                    conform_animation(  src_obj,
                                    tgt_obj,
                                    src_range=src_range,
                                    tgt_starting_frame = init_frame_current,
                                    actions = actions, 
                                    verbose = verbose, 
                                    fcurve_to_ignore = fcurves_to_ignore,
                                    src_scene = src_scene,
                                    )
                
                    if prop.flip :
                        apply_flip_transformation( src_obj,
                                                tgt_obj,
                                                src_range=src_range,
                                                tgt_starting_frame = init_frame_current,
                                                verbose = verbose,
                                                )
                    
                    # bpy.context.scene.frame_set(bpy.context.scene.frame_current +1) 
                    # bpy.context.scene.frame_set(bpy.context.scene.frame_current -1) 

                    ### END NEW ANIM CONFORMING



                    #print('import anim of', tgt_obj.name , time.time() - mid_time )
                    mid_time = time.time()
                                
                else:
                    print('no data to import on', tgt_obj.name)
                # bpy.context.view_layer.update()
                #print(tgt_obj.name, 'TOTAL =', time.time() - self.start_time)
        context.scene.frame_set(init_frame_current)
        return{'FINISHED'}

class GPCUTOUT_OT_MCMODAL(bpy.types.Operator):
    """Mastercontroller modal operator : imports data from template scene over selected objects, based on mouse position on a grid"""
    bl_idname = "gpcutout.mc_modal"
    bl_label = "Import Mastercontroller on selected objects"
    bl_options = {'UNDO'}
    layers_to_use = []
    src_root_objects = []
    root_objects = []

    #DICTIONNARY
    src_objects_keys_dic={}
    tgt_objects_keys_dic={}
    material_table_dic = {}

    
    # last_clip_gpframe = {}
    last_clip_frame_number = 0
    handler = []
    
    assetObject_name : StringProperty()
    asset_range = (None, None)
    asset_grid = (None, None)
    factor:FloatProperty(default = 2)
    first_value: FloatProperty()

    squareSize : IntProperty()
    center_x : IntProperty()
    center_y : IntProperty()

    mouseX : FloatProperty()
    mouseY : FloatProperty()
    clip_frame_i : IntProperty(default = -1)
    ctrl : BoolProperty(default = True)
    flip : BoolProperty(default = False)
    was_flipped : BoolProperty(default = False)
    clip_array_index : bpy.props.IntProperty(default = -1, options={'HIDDEN'})

    gp_fobidden_pattern = ['CTLR','CTRL','GLOBAL','FULLSIGHT','HLP']
    layer_forbidden_pattern = ['HELPERS']

    interpolation_key : StringProperty(default = 'LEFT_SHIFT')
    flip_key : StringProperty(default = 'F')

    keep_linked : BoolProperty(default = False)

    #Mouse coord cosnt
    minX = None
    maxX = None
    minY = None
    maxY = None
    nb_segments_x = None
    nb_segments_y = None

    previous_grid = []

    
    def modal(self, context, event):

        if event.type == 'MOUSEMOVE':
            self.interpolate(context, event)
        if event.type == self.interpolation_key :
            if event.value == 'PRESS':
                self.ctrl = False
            if event.value == 'RELEASE':
                self.ctrl = True
        #else if key f is hold flip = true and if release = false
        if event.type == self.flip_key :
            if event.value == 'PRESS':
                self.flip = True
            if event.value == 'RELEASE':
                self.flip = False
        elif event.type == 'LEFTMOUSE':
            if len(self.handler) > 0:
                try:
                        bpy.types.SpaceView3D.draw_handler_remove(self.handler[0], 'WINDOW')
                except:
                    pass
            for k in self.tgt_objects_keys_dic:
                # get obj
                tgt_obj = bpy.data.objects[k]
                #if its gp
                if tgt_obj and tgt_obj.type == 'GPENCIL' :
                    update_gp_points(tgt_obj.data)


                    #Final COLORMATCH
                    src_name = tgt_obj.name
                    #handle prefix
                    self.offset = get_unik_level(src_name)
                    src_name = src_name[self.offset:]

                    selectedAsset = context.window_manager.current_asset_action
                    src_scene = selectedAsset.import_data.src_scene
                    src_obj = src_scene.objects.get(src_name)
                    
                    material_table,missmatch  = colormatch_dic(src_obj, tgt_obj)
                    # conform_material_table(src_obj, tgt_obj)
                    
                    if missmatch:
                        for tgt_layer in tgt_obj.data.layers :
                            src_layer = get_gplyr_from_name(src_obj, tgt_layer.info)  
                            tgt_gpframe = get_now_gpframe(context.scene.frame_current, tgt_layer)
                            src_frame =  get_now_gpframe(src_scene.frame_current, src_layer)    
                            if tgt_gpframe is None or src_frame is None:  
                                continue                  
                            for i, tgt_stroke in enumerate(tgt_gpframe.strokes):
                                src_stroke = src_frame.strokes[i] if i < len(src_frame.strokes) else None
                                if src_stroke is not None:
                                    colormatch_from_table(src_stroke, tgt_stroke ,  material_table)
            if not self.keep_linked :
                print('Removing scene --', src_scene.name)
                bpy.data.scenes.remove(src_scene)

            return {'FINISHED'}
        
        elif event.type in ['RIGHTMOUSE', 'ESC']:
            #delete created keyframes :
            #TODO solve issue when abort after importing over existing keyframes
            # for object in self.tgt_gp_keys :
            #     for layer, frame in object[1] :
            #         layer.frames.remove(frame)
            # for object in self.tgt_curve_keys :
            #     for fcurve, keyframe in object[1] :
            #         fcurve.keyframe_points.remove(keyframe)
            #remove handlers
            if len(self.handler) > 0:
                try:
                        bpy.types.SpaceView3D.draw_handler_remove(self.handler[0], 'WINDOW')
                        bpy.types.SpaceView3D.draw_handler_remove(self.cadre[0], 'WINDOW')
                except:
                    pass
            
            bpy.ops.ed.undo()
            return {'CANCELLED'}
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        
        pseudo=False
        print('Master controller start')
        selectedAsset = context.window_manager.current_asset_action

        addon_prefs=None
        try:
            addon_prefs = context.preferences.addons['gp-cut-out'].preferences # BUG : addon_prefs is None when called from the asset manager
        except:
            print('addon prefs not found')
        
        
        #if selectedAsset.import_data.asset_type == 'MC' : #get data from the asset object
        if addon_prefs:
            self.interpolation_key = addon_prefs.interpolation_key
            self.flip_key = addon_prefs.flip_key

        src_scene = selectedAsset.import_data.src_scene
        self.asset_range = tuple(selectedAsset.import_data.range)
        self.asset_grid = tuple(selectedAsset.import_data.grid)
        
        self.mc_from_pose = False
        extend_grid_1 = False
        #check conformity of grid and range
        if selectedAsset.import_data.asset_type != 'MC' :
            print("it's a POSE. Preparing interpolation from current pose")
            self.asset_grid = (1,1)
            pseudo = True
            self.mc_from_pose = True
        elif self.asset_grid[0]*self.asset_grid[1] != self.asset_range[1] - self.asset_range[0] +1 :
            print('asset non conforme (grid size / range)')
            return {'CANCELLED'}         
        elif self.asset_grid[1] == 1 : #poses in line (not grid) we'll duplicate the key frame on a second line to have a grid
            self.asset_grid = (self.asset_grid[0], 2)
            extend_grid_1 = True
        #TODO vertical line

        print('prepare to import :', selectedAsset.name)

        self.root_objects.clear()
        self.src_objects_keys_dic.clear()
        self.tgt_objects_keys_dic.clear()
        self.material_table_dic.clear()
        # self.gp_interpolate_grids.clear()
        prop = context.scene.import_properties

        #DUPLICATA FROM IMPORT OPERATOR TODO : factorize
        if not prop.hard_import:                
            print('start soft import')    

            #get root objects (top parents)
            src_root_objects = []
            for src_root_obj in [ob.object for ob in selectedAsset.asset_root_objects if ob.object is not None] :
                src_root_objects.append(src_root_obj)
        else:
            print('start hard import')
            # get the object with name prop.possible_roots
            src_root_objects = []
            for ob in src_scene.objects:
                if ob.name == prop.roots_enum:
                    src_root_objects = [ob]
                    break

        
        # determine the root objects, according to asset root ans selected objects
        self.root_objects, objects_to_process = prepare_to_import_objects(
                                                        context, 
                                                        src_root_objects, 
                                                        src_scene,
                                                        hard_import=prop.hard_import,
                                                        selected_only = prop.selected_only,
                                                        use_current_collection = prop.use_current_collection,
                                                        )
        if not objects_to_process or len(objects_to_process) == 0 :
            self.report({"WARNING"}, "Import: No object to process")
            return {'CANCELLED'}
        
        found_objs = 0
        update_frame = False
        current_frame = context.scene.frame_current

        # Building Fcurves and GP keyframes lists
        for obj_ind, tgt_obj in enumerate(objects_to_process) :

           

            is_root = False
            if tgt_obj in self.root_objects :
                is_root = True

            src_name = tgt_obj.name
            #handle prefix
            self.offset = get_unik_level(src_name)
            src_name = src_name[self.offset:]

            src_obj = src_scene.objects.get(src_name)

            
            if src_obj is None :
                print('WARNING! object ' + tgt_obj.name + 'was not found')
                if tgt_obj.type == 'GPENCIL' :
                    #add blank keyframe in every layer - store new frames for abort
                    for layer in tgt_obj.data.layers :

                        #check if there's already a keyframe and remove it
                        del_kf = get_now_gpframe(current_frame, layer)
                        if del_kf :
                            layer.frames.remove(del_kf)

                        blank_frame = layer.frames.new(current_frame)
                        layer.frames.update()
                        #self.tgt_gp_keys[obj_ind][1].append((tgt_layer, new_frame))
                    pass
            else :
                material_table,missmatch  = colormatch_dic(src_obj, tgt_obj)
                # conform_material_table(src_obj, tgt_obj)
                if missmatch:
                    self.material_table_dic[tgt_obj.name] = material_table


                found_objs += 1
                # BUILDING SRC KEY DIC
                src_key_dic =  get_animation_kf_dic(src_obj,
                                                                                cible=['FC','GP'],
                                                                                frame_range = self.asset_range,
                                                                                extend_grid_1=extend_grid_1,
                                                                                verbose=verbose,
                                                                                pseudo=pseudo
                                                                            #  gp_fobidden_pattern=self.gp_fobidden_pattern,
                                                                                )
                if self.mc_from_pose:
                    enforce_keyframe(tgt_obj, current_frame)
                    current_pose_key_dic= get_animation_kf_dic(tgt_obj,
                                                                                cible=['FC','GP'],
                                                                                frame_range = (current_frame,current_frame+1),
                                                                                extend_grid_1=extend_grid_1,
                                                                                verbose=verbose,
                                                                                pseudo=pseudo,
                                                                                # offset = 2,
                                                                            #  gp_fobidden_pattern=self.gp_fobidden_pattern,
                                                                                )
                    self.asset_grid = tuple((2,1))
                    src_key_dic = recursive_merge_dict(src_key_dic,current_pose_key_dic)

                self.src_objects_keys_dic[src_obj.name] = src_key_dic                                                                
                self.tgt_objects_keys_dic[tgt_obj.name] = {'FC':{},'GP':{}}

                # NEW GETTING TGT KEYS
                # SET INITIAL TGT KF
                src_fcurves_dic = self.src_objects_keys_dic[src_obj.name]['FC']
                tgt_fcurves_dic = self.tgt_objects_keys_dic[tgt_obj.name]['FC']
                for src_data_path, src_fcurves_array_dic in src_fcurves_dic.items():                    
                    tgt_fcurves_array_dic = get_from_dic_or_create(tgt_fcurves_dic,src_data_path,{})

                    for src_array_index, src_kf_list in src_fcurves_array_dic.items(): 
                        tgt_kf_list = get_from_dic_or_create(tgt_fcurves_array_dic,src_array_index,[])                           
                        
                        tgt_fcurve = get_fc(tgt_obj,src_data_path,src_array_index)
                        if tgt_fcurve is None:                            
                            print('fcurve %s doesnt exist on tgt object %s'%(src_data_path,tgt_obj.name))
                            continue
                        
                        middleframe_index = round(len(src_kf_list)/2)
                        src_kf = src_kf_list[middleframe_index]                                
                        tgt_kf = get_now_kf(context.scene.frame_current, tgt_fcurve)
                        
                        if tgt_kf is None:
                            if is_root:
                                coord_cible = tgt_fcurve.evaluate(context.scene.frame_current)
                            else:
                                coord_cible = src_kf.co.y

                            tgt_kf = tgt_fcurve.keyframe_points.insert(context.scene.frame_current,coord_cible)                                    
                            tgt_kf.interpolation = 'CONSTANT'                                    
                        
                        #STORE TGT KF
                        tgt_kf_list.append(tgt_kf)
                        self.tgt_objects_keys_dic[tgt_obj.name]['FC'][src_data_path][src_array_index] = tgt_kf_list
                # SET INITIAL TGT GP KF
                src_gp_keys_dic = self.src_objects_keys_dic[src_obj.name].get('GP',{})
                # tgt_gp_keys_dic = self.tgt_objects_keys_dic[tgt_obj.name]['GP']
                    #SET TGT INITIAL FRAME
                layer_index = 0
                for src_layer_name, src_layer_gp_keys in src_gp_keys_dic.items():
                    
                    tgt_layer = get_gplyr_from_name(tgt_obj,src_layer_name) 
                    src_layer = get_gplyr_from_name(src_obj,src_layer_name)                 
                    
                    if tgt_layer is not None and not tgt_layer.lock:
                        # check if the number of frames is the same as the grid
                        sourceframe_number = len(src_layer.frames)
                        grid_number = (self.asset_grid[1]-1)*self.asset_grid[0]
                        if sourceframe_number < grid_number :
                            print('WARNING! number of frames in source layer %s is inferior to grid slots'%(src_layer.info))
                            print('source layer has %s frames, grid has %s frames'%(sourceframe_number, grid_number))
                            #adding first gpFrame if there isn't in current
                            if not tgt_layer.frames :
                                conform_animation(src_obj,tgt_obj,
                                                    src_range=[1,2],
                                                    tgt_starting_frame = context.scene.frame_current,
                                                    actions = ['GPENCIL_FRAMES_COPY'], 
                                                    verbose = verbose, 
                                                    fcurve_to_ignore = [])
                            continue 
                        

                        
                            
                        
                        #Check if need update (lastlayer lastframe)
                        if obj_ind == len(objects_to_process)-1 and layer_index == len(src_obj.data.layers)-1 :
                            update_frame = True
                        layer_index += 1

                        
                        middleframe_index = round(len(src_layer_gp_keys)/2) 
                        src_frame = src_layer_gp_keys[middleframe_index]
                        tgt_gpframe = get_now_gpframe(context.scene.frame_current, tgt_layer)
                                                            
                        if tgt_gpframe is None :  
                            if pseudo:
                                #insert a frame
                                tgt_gpframe = tgt_layer.frames.new(context.scene.frame_current)
                                frame_confo(src_frame, tgt_gpframe,keep=True, verbose = False)
                                # self.tgt_objects_keys_dic[tgt_obj.name]['GP'][tgt_layer_name] = tgt_keyframe 
                            else:
                                tgt_gpframe = tgt_layer.frames.copy(src_frame)
                                tgt_gpframe = move_gp_frame(tgt_layer, 
                                                            tgt_gpframe, 
                                                            context.scene.frame_current,
                                                            update=update_frame)
                                #tgt_gpframe.frame_number = context.scene.frame_current
                                # colormatch(src_obj, src_frame, tgt_obj, tgt_gpframe )
                                #tgt_gpframe.frame_number = context.scene.frame_current

                        if missmatch:
                            for i, tgt_stroke in enumerate(tgt_gpframe.strokes):
                                src_stroke = src_frame.strokes[i] if i < len(src_frame.strokes) else None
                                if src_stroke is not None:
                                    colormatch_from_table(src_stroke, tgt_stroke ,  material_table)

                        #STORE CIBLE    
                        
                        self.tgt_objects_keys_dic[tgt_obj.name]['GP'][src_layer_name] = tgt_gpframe
                        # self.last_clip_gpframe[tgt_layer] = src_frame
                        
                for pattern in self.gp_fobidden_pattern :
                    if re.search(pattern, src_obj.name) :
                        print('obj %s is forbidden'%(src_layer_name))
                        self.tgt_objects_keys_dic[tgt_obj.name]['GP'] = {}

                        break    


        


        if found_objs > 0 : 
            #count elements to interpolate :
            layers = 0
            # for item in self.tgt_gp_keys : 
            # for item in self.tgt_objects_keys_dic.get('GP',{}) :
            #     layers += len(item)
            # fcurves = 0
            # for item in self.tgt_curve_keys :
            #     fcurves += len(item[1])
            # print(layers, 'layers ---', fcurves, 'fcurves')

            # Get the size of the viewport
            width = context.region.width
            height = context.region.height
            # Calculate the center of the viewport
            self.center_x = width // 2
            self.center_y = height // 2

            


            # check the biggest value so the square is always a square
            if width > height:
                self.squareSize = (height- (60*height)//100)
            else:
                self.squareSize = (width -(60*width)//100)

            #self.clip_array_index = -1 #it already is
            if addon_prefs:
                if addon_prefs.switch_depth_location :
                    if addon_prefs.depth_axis == 'X' :
                        self.clip_array_index = 0
                    if addon_prefs.depth_axis == 'Y' :
                        self.clip_array_index = 1
                    if addon_prefs.depth_axis == 'Z' :
                        self.clip_array_index = 2 
            else :
                self.clip_array_index = 1 #default to Y

            print('go modal') #run modal()
            #return {'FINISHED'} #TEST INVOKE ONLY
            context.window_manager.modal_handler_add(self)
            self.handler.clear()
            handler = bpy.types.SpaceView3D.draw_handler_add(self.draw_callback_px, (event, None), 'WINDOW', 'POST_PIXEL')
            self.handler.append(handler)
            return {'RUNNING_MODAL'}
        else : #no correspondancy between selected objects and src scene
            #TODO ask to create every object of thte template (pick a collection?)
            return {'CANCELLED'}
        
 
    def interpolate(self, context, event) :
        clip =  self.ctrl
        if self.mc_from_pose:
            clip = not clip
        
        if self. nb_segments_y is None:
            if context.region is None:
                return {'FINISHED'}
            self.minX = (self.center_x + context.region.x)-self.squareSize
            self.maxX = (self.center_x + context.region.x)+self.squareSize
            self.minY = (self.center_y + context.region.y)-self.squareSize
            self.maxY = (self.center_y + context.region.y)+self.squareSize
            self.nb_segments_x = self.asset_grid[0]-1
            self.nb_segments_y = self.asset_grid[1]-1 

        verbose = False #COMMENT FOR DEBUG        
        
        tempMouseX = min(self.maxX, max(self.minX, event.mouse_x))        
        tempMouseY = min(self.maxY, max(self.minY, event.mouse_y))
        
        mouseX = self.map(tempMouseX, self.minX, self.maxX, 0, 1)
        mouseY = self.map(tempMouseY, self.minY, self.maxY, 0, 1)

        #get magnitude between self.mouse and mouse
        magnitude = math.sqrt((self.mouseX-mouseX)**2+(self.mouseY-mouseY)**2)

        if magnitude > 0.005 or  self.was_flipped!=self.flip:
            loop_time = time.time()
            full_gp = full_curve =0.0

            if verbose:                     
                print('vector is long enough', magnitude)

            self.mouseX = mouseX
            self.mouseY = mouseY
            
            # invert mouseY because origin (KF[0] is at top left in the templates
            mouseY = 1-mouseY 
            
            #DEFINE XY POSITION IN THE GRID
            for seg_x in range(self.nb_segments_x) :
                seg_start = (1/self.nb_segments_x)*seg_x
                seg_stop = (1/self.nb_segments_x)*(seg_x+1)
                if seg_start <= mouseX <= seg_stop :
                    pos_x = seg_x
                    mouseX = self.map(mouseX, seg_start, seg_stop, 0, 1)
                    break
            pos_y = 0
            for seg_y in range(self.nb_segments_y) :
                seg_start = (1/self.nb_segments_y)*seg_y
                seg_stop = (1/self.nb_segments_y)*(seg_y+1)
                if seg_start <= mouseY <= seg_stop :
                    pos_y = seg_y
                    mouseY = self.map(mouseY, seg_start, seg_stop, 0, 1)
                    break

            # DEFINE THE SURROUNDING FRAME INDEX IN THE GRID
            # ind_TL = pos_x + (pos_y * self.asset_grid[1])
            ind_TL = pos_x + (pos_y * self.asset_grid[0])
            ind_DL = pos_x + (pos_y + 1) *self.asset_grid[0] 
            ind_DR = (pos_x + 1) + (pos_y + 1) * self.asset_grid[0]
            ind_TR = (pos_x + 1) + pos_y * self.asset_grid[0] 
            if self.mc_from_pose:
                surrounding_frame_i = [ind_TL,ind_TR]
            else:
                surrounding_frame_i = [ind_TL,ind_DL,ind_DR,ind_TR]
            

            
            coeff_list = self.getCoeff(mouseX, 1-mouseY, line = self.mc_from_pose)
            #create numpy array of coefficients
            np_coeff_array = np.array(coeff_list)
            #get the index of the biggest coefficient
            higher_coeff_index = coeff_list.index(max(coeff_list))
            
            clip_frame_i = surrounding_frame_i[higher_coeff_index]
            
            # TODO If doesnt change from previous and interpolate off, should stop here
            # But this do cause bugs
            force_clip = False
            if clip: 
                if self.clip_frame_i == clip_frame_i :
                    return {'FINISHED'}                    
                else:
                    if verbose: print('current clip frame',self.clip_frame_i,'new clip frame', clip_frame_i, 'from', surrounding_frame_i, 'with', coeff_list)
                    self.clip_frame_i = clip_frame_i
                    force_clip = True
            #TODO improve, not good enough
            # else:                
            #     if self.previous_grid!=surrounding_frame_i:
            #         self.previous_grid = surrounding_frame_i
            #         if clip_frame_i != self.clip_frame_i:
            #             self.clip_frame_i = clip_frame_i
            #             force_clip= True

           
           
            if verbose: print('loop setup', time.time()-loop_time)
            update = False
            for tgt_obj_name, tgt_keyframe_dic in self.tgt_objects_keys_dic.items() :
                getting_time = time.time()


                # GET SRC OBJECTS
                if not self.flip:
                    src_name = tgt_obj_name
                else:
                    suffix_ind =  regex_get_lateral_index(tgt_obj_name)
                    if suffix_ind == None:
                        continue
                    src_name=regex_flip_name(tgt_obj_name)
                    
                    result = {}#to hold flip result
                
                #prefix handle    
                src_name = src_name[self.offset:]

                src_keyframe_dic = self.src_objects_keys_dic.get(src_name)
                if src_keyframe_dic == None:
                    continue  


                tgt_obj = bpy.data.objects[tgt_obj_name]
                material_table = self.material_table_dic.get(tgt_obj_name)

                tgt_fcurves_dic = tgt_keyframe_dic.get('FC',{})
                src_fcurves_dic = src_keyframe_dic.get('FC',{})

                if verbose: 
                    if verbose: print('object getting time', time.time()-getting_time)
                    fcurve_time = time.time()
                    
                for tgt_fcurve_path in tgt_fcurves_dic.keys() : 

                    tgt_keyframe_array_dic = tgt_fcurves_dic.get(tgt_fcurve_path)
                    src_keyframe_array_dic = src_fcurves_dic.get(tgt_fcurve_path)
                    if tgt_keyframe_array_dic == None or src_keyframe_array_dic == None:
                        continue
                    for tgt_array_index in tgt_keyframe_array_dic.keys() : 

                        
                        
                        
                        if ( tgt_obj not in self.root_objects 
                            or (tgt_obj in self.root_objects 
                                and tgt_fcurve_path not in ['location', 'rotation_euler', 'scale']
                                )
                            ):          

                            src_curve_key_list = src_keyframe_array_dic[tgt_array_index]
                            tgt_curve_key_list = tgt_keyframe_array_dic[tgt_array_index]
                            if len(tgt_curve_key_list) <=0 :
                                continue
                            tgt_keyframe = tgt_curve_key_list[0]

                            if len(src_curve_key_list) <=0 :
                                continue
                            curve_clip = clip
                            #WHATFOR
                            # if (self.clip_array_index != -1
                            #     and tgt_fcurve_path == 'location'
                            #     and tgt_array_index == self.clip_array_index 
                            #     ) :
                            #     curve_clip = True
                            # interpolate_fcurve()
                            tgt_keyframe.co.y = interpolate_co_y(src_curve_key_list, 
                                                                 surrounding_frame_i,
                                                                 coeff_list,
                                                                 curve_clip,
                                                                 )
                            if self.flip:
                                result |=flip_fcurve_kf(tgt_keyframe,
                                                        tgt_fcurve_path,
                                                        tgt_array_index,
                                                        suffix_ind)
                                                
                            
                if verbose:
                    print ('fcurve update time',time.time() - fcurve_time)
                    full_curve +=time.time() - fcurve_time
                    gp_time = time.time()
                # Grease pencil interpolation
                # GP_TL =  GP_DL = GP_DR = GP_TR = None                
                update = False   

                tgt_gp_layers_dic = tgt_keyframe_dic.get('GP',{})
                src_gp_layers_dic = src_keyframe_dic.get('GP',{})

                for tgt_layer_name, tgt_keyframe in tgt_gp_layers_dic.items() :                    
                    src_gp_key_list = src_gp_layers_dic[tgt_layer_name]

                    tgt_layer = get_gplyr_from_name(tgt_obj, tgt_layer_name)
                    
                    # last_clip_gpframe = self.last_clip_gpframe[tgt_layer]
                    clip_gpframe = src_gp_key_list[clip_frame_i]

                    
                    #New 
                    keyframe_list = [ src_gp_key_list[i] for i in surrounding_frame_i]                                       
                        
                    if (not clip #and clip_gpframe == last_clip_gpframe
                        and not force_clip
                        ) :
                        #interpolate Gpencil points
                        layer_interpolation(tgt_keyframe, keyframe_list, coeff_list, verbose = False)
                        

                        # for k, stroke in enumerate(tgt_keyframe.strokes ):
                        #     #NEW 
                        #     #check if strokes length is > k  in all gp_list
                        #     # if not all(len(gp.strokes) > k for gp in gp_list):
                        #     #     continue

                        #     stroke_list = [gp.strokes[k] if len(gp.strokes) > k else None for gp in gp_list  ]   

                        #     #FACTORIZATION OF OLD
                        #     stroke_interpolation(stroke, stroke_list, coeff_list, verbose = False)                    

                            
                    
                    elif (
                          self.was_flipped!=self.flip 
                          or force_clip
                        #   or clip_frame_i != self.clip_frame_i
                          ): 
                        self.was_flipped = self.flip
                        update = True                                
                        
                        #HARD CONFO V
                        if self.mc_from_pose:                        
                            frame_confo(clip_gpframe, tgt_keyframe,keep=True, verbose = False)
                            # self.tgt_objects_keys_dic[tgt_obj.name]['GP'][tgt_layer_name] = tgt_keyframe 
                        
                        #MESS COPY V
                        else:
                            tgt_gpframe = move_gp_frame(tgt_layer, 
                                                        tgt_layer.frames.copy(clip_gpframe), 
                                                        context.scene.frame_current,
                                                        update = False)          
                            self.tgt_objects_keys_dic[tgt_obj.name]['GP'][tgt_layer_name] = tgt_gpframe              
                        

                        #NEW                       
                        # self.last_clip_gpframe[tgt_layer] = clip_gpframe   
                                                 
                    # if (material_table is not None 
                    #     and tgt_keyframe is not None
                    #     and hasattr(tgt_keyframe, 'strokes')):

                    #     strokes_list = tgt_keyframe.strokes
                    #     if strokes_list is None:
                    #         if verbose:print("strokes_list is None")
                    #     else:
                    #         # try:
                    #         #     stroke_list_len = len(strokes_list)
                    #         # except Exception as e:
                    #         #     print(e)
                    #         #     strokes_list_len = 0
                    #         # for i in range(stroke_list_len):
                    #         temp_strokes_list = list(strokes_list) # THE BUG LINE

                    #         for i, tgt_stroke in enumerate(temp_strokes_list):
                    #             # tgt_stroke = tgt_keyframe.get(key)

                    #             src_stroke = keyframe_list[higher_coeff_index].strokes[i] if i < len(keyframe_list[higher_coeff_index].strokes) else None
                                
                    #             if src_stroke is not None:
                    #                 colormatch_from_table(src_stroke, tgt_stroke ,  material_table)
                    #                 # update = False
                    #                 pass


                    is_layer_flippable = not any([re.search(pattern, tgt_layer_name) for pattern in self.layer_forbidden_pattern])
                    if (self.flip 
                        and update 
                        and is_layer_flippable):
                                            
                        flip_layer_frame(tgt_layer,
                                         context.scene.frame_current,
                                         suffix_ind,
                                         verbose=False,**result)       
                         
                # self.last_clip_gpframe = surrounding_frame_i[higher_coeff_index]
                if verbose:
                    print ('gp update time',time.time() - gp_time)
                    full_gp +=time.time() - gp_time
            if update:
                update_gp_frame_display()
            if verbose:
                print ('update loop time',time.time() - loop_time)
                print ('full curve time',full_curve)
                print ('full gp time',full_gp)
        return {'FINISHED'}
    
    
    def map(self,value, leftMin, leftMax, rightMin, rightMax):
        leftSpan = leftMax - leftMin
        rightSpan = rightMax - rightMin
        valueScaled = float(value - leftMin) / float(leftSpan)
        return rightMin + (valueScaled * rightSpan)
    
    def getCoeff(self, x, y,line = False):
        coef_TL = abs(x-1) * abs(y-0)
        coef_DL = abs(x-1) * abs(y-1)
        coef_DR = abs(x-0) * abs(y-1)
        coef_TR = abs(x-0) * abs(y-0)
        if line :
            coef_list = [coef_TL+coef_DL,coef_DR+coef_TR]
        else:
            coef_list = [coef_TL,coef_DL,coef_DR,coef_TR]
        
        return coef_list

    def draw_callback_px(self, a, b):
        """Draws list of available controls for transformation mode."""
        font_id = 0
        blf.color(font_id, 1.0, 0.36, 0.36, 1.0)
        blf.size(font_id, 15, 72)
        blf.position(font_id, 680, 100, 0)
        blf.draw(font_id, "Master controller modal ")
        blf.position(font_id, 680, 85, 0)
        blf.size(font_id, 10, 72)
        blf.draw(font_id, "Interpolate : %s"%self.interpolation_key)
        blf.position(font_id, 680, 70, 0)
        blf.draw(font_id, "Flip : %s"%self.flip_key)
        bpy.context.view_layer.update()
