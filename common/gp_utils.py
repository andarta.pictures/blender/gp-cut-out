import bpy
from mathutils import Matrix,Vector
###PSPEUDO CLASSES

class Pseudo_point:
    """store point data without it being a blender point"""
    co=None
    pressure=None
    select=None
    strength=None
    uv_factor=None
    uv_fill=None
    uv_rotation=None
    vertex_color=None
    def __init__(self, src_pt=None):
        if src_pt is not None:
            point_confo(src_pt, self)

    def __repr__(self):
        return 'pseudo_point(%s, %s, %s)'%(self.co, self.pressure, self.strength)

    def __str__(self):
        return 'pseudo_point(%s, %s, %s)'%(self.co, self.pressure, self.strength)

    def __eq__(self, other):
        if isinstance(other, Pseudo_point):
            return self.co == other.co and self.pressure == other.pressure and self.strength == other.strength
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.co, self.pressure, self.strength))

class Pseudo_points:
    points = []
    def __init__(self, src_points=[]):
        self.points = []
        for p in src_points:
            self.points.append(Pseudo_point(p))

    def __iter__(self):
        for p in self.points:
            yield p

    def __getitem__(self, item):
        return self.points[item]
    
    def __len__(self):
        return len(self.points)
    
    def __hash__(self):
        return hash(tuple(self.points))
    
    def new(self):
        self.points.append(Pseudo_point())

    def add(self, count=1):
        for i in range(count):
            self.points.append(Pseudo_point())

    def pop(self, index=-1):
        return self.points.pop(index)

    def remove(self, point):
        try:
            self.points.remove(point)
        except:
            pass

class Pseudo_stroke:
    """store stroke data without it being a blender stroke"""
    aspect=None
    display_mode=None
    end_cap_mode=None
    hardness=None
    is_nofill_stroke=None
    line_width=None
    material_index=None    
    select=None
    select_index=None
    start_cap_mode=None
    use_cyclic=None
    uv_rotation=None
    uv_scale=None
    uv_translation=None
    vertex_color_fill=None
    points: Pseudo_points

    def __init__(self, src_stroke=None):
        self.points = Pseudo_points()
        if src_stroke is not None:
            stroke_confo(src_stroke, self)
    def __str__(self) -> str:
        return 'pseudo_stroke(%s, %s, %s)'%(self.points, self.line_width, self.material_index)
        pass
    
class Pseudo_Strokes:
    strokes = []
    def __init__(self, src_strokes=[]):
        self.strokes = []
        for s in src_strokes:
            self.strokes.append(Pseudo_stroke(s))

    def __iter__(self):
        for s in self.strokes:
            yield s
            
    def __len__(self):
        return len(self.strokes)
    
    def __getitem__(self, item):
        return self.strokes[item]
    
    def new(self):
        new_stroke = Pseudo_stroke()
        self.strokes.append(new_stroke)
        return new_stroke

    def remove(self, stroke):
        try:
            self.strokes.remove(stroke)
        except:
            pass

class Pseudo_gp_frame:
    '''store gp frame data without it being a blender gp frame'''
    strokes : Pseudo_Strokes
    keyframe_type=None
    def __init__(self, src_gpframe):
       self.strokes = Pseudo_Strokes()
       frame_confo(src_gpframe, self)
    


### GENERAL
def get_layer_index(layer) :
    '''get the index of a given layer
    args : layer = the given layer
    returns : the index as int'''   
    for i, l in enumerate(layer.id_data.layers) :
        if l == layer :
            return i

def get_now_gpframe(current_frame, layer):
    for frame in layer.frames :
        if frame.frame_number == current_frame :
            return frame
    return None

def get_active_gpframe(current_frame, layer):
    result_frame = None
    for frame in layer.frames :
        if frame.frame_number > current_frame :
            break
        result_frame = frame       
    return result_frame

def get_gplyr_from_name(gp, name):
    '''get a layer from a grease pencil by its name'''
    for tgt_layer in gp.data.layers:
        if name == tgt_layer.info:
            return tgt_layer   
    return None     

def get_gplyr_from_stroke(stroke):
    '''get the gplayer of a given stroke'''
    for layer in bpy.data.grease_pencil[stroke.grease_pencil].layers:
        for s in layer.strokes:
            if s == stroke:
                return layer
       
    
def move_layer_layer_to_index(layer, index):
    '''moves a layer in the layer's list to give him the specified index
    args : layer = the layer to move
           index = the index to match'''
    layer_index = get_layer_index(layer)
    if layer_index > index :
        for i in range(layer_index - index):
            layer.id_data.layers.move(layer, 'DOWN')
    if layer_index < index :
        for i in range(index - layer_index):
            layer.id_data.layers.move(layer, 'UP')

def sync_layers(src_gp, gp) :
    '''look for missing layers in destination grease pencil (compnewared to a given source object)
    assume that layers haven't been switched compared to source object
    args : src_gp = source grease pencil
           gp = destination grease pencil'''
    new_layers = []
    for i, src_layer in enumerate(src_gp.layers) :
        if src_layer.info not in [layer.info for layer in gp.layers] :
            new_layer = gp.layers.new(src_layer.info)
            #TODO : check layer attributes and relations (parentship, trnasforms, masking relations)
            for attr in dir(src_layer): 
                try:
                    setattr(new_layer, attr, getattr(src_layer, attr)) 
                except:
                    pass
            new_layers.append(new_layer)
            #reorder layer
            if i == 0 :
                move_layer_layer_to_index(new_layer, 0)
            else : 
                previous_layer = src_gp.layers[i-1]
                if previous_layer.info in [layer.info for layer in gp.layers] :
                    before_index = get_layer_index(gp.layers[previous_layer.info])
                    move_layer_layer_to_index(new_layer, before_index + 1)
                else : 
                    move_layer_layer_to_index(new_layer, i)
            if src_layer.parent and src_layer.parent_type == 'OBJECT':  
                try :
                    new_layer.parent = bpy.data.objects[src_layer.parent.name]
                    new_layer.matrix_inverse = src_layer.matrix_inverse
                except :
                    pass

### ANIMATION
def move_gp_frame(layer, frame, frame_number,update = True):
    '''move a gp frame along the timeline
    the main issue of this method it that every frame of the layer are copies, and direct references to them will now lead to an error
    args : layer = layer whare to move the keyframe
           frame = frame to move
           frame_number = where to move the frame
    returns : the moved frame (note that references to other frames will be corrupted)
    '''
    temp_layer = frame.id_data.layers.new('temp')
    for src_frame in layer.frames : 
        temp_fr = temp_layer.frames.copy(src_frame)
        if src_frame == frame :
            temp_fr.frame_number = frame_number      
    d = dict()    
    for i, temp_fr in enumerate(temp_layer.frames) :
        d[temp_fr.frame_number] = i
    layer.clear()  
    for key in sorted(d) :
        i = d[key]
        new_fr = layer.frames.copy(temp_layer.frames[i])
        if new_fr.frame_number == frame_number :
            moved_frame = new_fr  
    layer.id_data.layers.remove(temp_layer) 
    #still need to kind of refresh by frame change
    if update:
        update_gp_frame_display()
    return moved_frame

def update_gp_frame_display():
        bpy.context.scene.frame_set(bpy.context.scene.frame_current +1) 
        bpy.context.scene.frame_set(bpy.context.scene.frame_current -1)    
        
def sculpted_frames(frame_1, frame_2):
    '''compare 2 frames to know if they can be interpolated (same amount of strokes and points in every stroke)
    args : frame_1 and frame_2 are the GPencil frames you want to compare
    returns : 'HOLD' if the frames are exactly the same
              'SCULPTED' if the frames are sculpted from each other and should be interpolated
              'DIFFERENT' if they don't have the same amount of strokes/points
    '''
    if len(frame_1.strokes) == len(frame_2.strokes):
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]
            if len(stroke_1.points) != len(stroke_2.points) :
                return 'DIFFERENT' #not the same amount of points in a stroke
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]
            for j, point_1 in enumerate(stroke_1.points) :
                point_2 = stroke_2.points[j]
                if point_1.co != point_2.co :
                    return 'SCULPTED'
        return 'HOLD' #not sculpted but exactly the same
    else :
        return 'DIFFERENT'

def get_interpolate_grid_value(frames):
    '''determines if a group of Gpencil frames (usually 4) should be interpolated
    args : frames = a list of frames
    returns : True or False denpending on the result'''
    result_list=[]
    for frame_1 in frames :
        for frame_2 in frames :
            if frame_1 != frame_2 :
                result_list.append(sculpted_frames(frame_1, frame_2))
    if 'DIFFERENT' in result_list :
        return False #can't interpolate with different frames
    else:
        if 'SCULPTED' not in result_list :
            return False #every frames are the same, the layer stays still, no need to interpolate
        else : 
            return True

### MATERIAL
def sync_materials(src_ob, new_ob, verbose = False):
    '''make sure that a new Grease Pencil object has all the materials imported from a source object
    args : src_ob = source object, Grease pencil you're importing from
           new_ob = destination object, grease pencil you're importing on'''
    for src_mat in [mat for mat in src_ob.data.materials if mat is not None] :
        if src_mat.library and src_mat.library == src_ob.library : #template local material 
            if src_mat.name not in [mat.name for mat in new_ob.data.materials if mat is not None]:
                if src_mat.name not in [mat.name for mat in bpy.data.materials if mat.library == None]:
                    new_mat = src_mat.copy()
                    new_mat.name = src_mat.name 
                    if verbose : print('new material created : ', new_mat.name)
                else :
                    new_mat = bpy.data.materials[src_mat.name, None]
                    if verbose : print('material already exists : ', new_mat.name)
                new_ob.data.materials.append(new_mat) 
        elif src_mat.library and src_mat.library != src_ob.library : #template linked material
            if src_mat.name not in [mat.name for mat in new_ob.data.materials if mat is not None] :
                if verbose : print('material %s not found in %s, trying to link it' % (src_mat.name,str(new_ob.data.materials)))
                new_ob.data.materials.append(src_mat)
            if verbose : print('material %s is already linked' % src_mat.name)
        
def frame_confo(src, tgt, keep=False, verbose = False):
    '''Conform a frame to another without using the blender copy method'''
    #keyframe_type
    tgt.keyframe_type = src.keyframe_type
    i=0
    for src_stroke in src.strokes :
        #search if existing stroke else create new
        if keep and len(tgt.strokes) > i :
            tgt_stroke = tgt.strokes[i]
            i+=1
        else:
            tgt_stroke = tgt.strokes.new()
        stroke_confo(src_stroke, tgt_stroke,keep=keep,verbose=verbose)
    if keep :
        for stroke in tgt.strokes[i:] :
            tgt.strokes.remove(stroke)
    return tgt

def stroke_confo(src, tgt,keep=False, verbose = False,
                  attributes = ['aspect',
                                'display_mode',
                                'end_cap_mode',
                                'hardness',
                                'is_nofill_stroke',
                                'line_width',
                                'material_index',
                                'select',
                                'select_index',
                                'start_cap_mode',
                                'use_cyclic',
                                'uv_rotation',
                                'uv_scale',
                                'uv_translation',
                                'vertex_color_fill',
                                ],
                                conform_points = True,
                                **kwargs):
    '''Conform a stroke to another without using the blender copy method''' 
    attributes = kwargs.get('stroke_attributes',attributes)

    for a in attributes :
        match a :
            # case 'force_constant_interpolation':
            #     #set frame interpolation to constant
            #     tgt.interpolation = 'CONSTANT'
            case _:
                try:
                    setattr(tgt, a, getattr(src, a))
                except:
                    if verbose : print("Confo strokes point failed on %s, attribute %s"%(tgt,a) )         
    
    if keep:
        count = len(tgt.points)
        if count < len(src.points):
            tgt.points.add(count = len(src.points)-count)
        elif count > len(src.points):
            for i in range(count-len(src.points)):
                tgt.points.pop()
    else:
        tgt.points.add(count = len(src.points))
        
    if conform_points:
        for i,point in enumerate(src.points):        
            point_confo(src.points[i], tgt.points[i], verbose=verbose, **kwargs)
    
def point_confo(src, tgt, attributes=['co',
                                    'pressure',
                                    'select',
                                    'strength',
                                    'uv_factor',
                                    'uv_fill',
                                    'uv_rotation',
                                    'vertex_color'],
                verbose = False,
                **kwargs):
    '''Conform a point to another without using the blender copy method''' 

    attributes = kwargs.get('point_attributes',attributes) 

    for a in attributes :
        match a :
            # case 'force_constant_interpolation':
            #     #set frame interpolation to constant
            #     tgt.interpolation = 'CONSTANT'
            case 'co':
                tgt.co = src.co.copy()
            case _:
                try:
                    setattr(tgt, a, getattr(src, a))
                except:
                    if verbose : print("Confo strokes point failed on %s, attribute %s"%(tgt,a) )        
#
def update_gp_points(gp):
    '''call update() on every points of a grease pencil'''
    for layer in gp.layers :
        for frame in layer.frames :
            for stroke in frame.strokes :
                stroke.points.update()

def colormatch_dic(src_ob, tgt_ob):
    '''build a correspondancy table of materials between two objects (src and tgt)
    format : {src_index : tgt_index}'''
    result = {}
    missmatch = False
    for src_i, src_slot in enumerate(src_ob.material_slots) :
        src_mat = src_slot.material
        tgt_i = None
        if src_mat in [m for m in tgt_ob.data.materials] :
            tgt_i = [i for i, slot in enumerate(tgt_ob.material_slots) if slot.material == src_mat][0]
        result[src_i] = tgt_i
        if src_i != tgt_i :
            missmatch = True
    return result,missmatch

def conform_material_table(src_ob, tgt_ob):
    '''Reorder the materials of tgt_ob to match the order of src_ob'''

    coresp_dic, missmatch = colormatch_dic(src_ob, tgt_ob)

    to_reindex = []
    if missmatch:
        # Build a list of the materials in src_ob in the correct order
        # src_materials = [slot.material for slot in src_ob.material_slots]

        # Build a list of the materials in tgt_ob
        tgt_materials_keys = [slot.material.name if slot.material is not None else None for slot in tgt_ob.material_slots]


        for i,tgt_key in enumerate(tgt_materials_keys):
    #   for i, mat_slot in enumerate(tgt_ob.material_slots):
                        
            true_index = coresp_dic.get(i)  
            if tgt_key is None or true_index == i:
                continue          
            if true_index is not None:
                key =  tgt_materials_keys[true_index]
                try:
                    if key is not None:
                        material = bpy.data.materials.get(key)
                    else:
                        material = None
                    tgt_ob.material_slots[i].material = material
                except Exception as e:
                    print('Error while reindexing materials:', key)
                    print(e)
                    
                #tgt_ob.material_slots[true_index].material = bpy.data.materials.get(tgt_key)
            else:
                to_reindex.append( bpy.data.materials.get(tgt_key) ) 
        
        # Reindex the materials in the list in index after reindexed
        for i, mat in enumerate(to_reindex):
            tgt_ob.material_slots[i+len(tgt_materials_keys)-len(to_reindex)].material = mat

def colormatch_from_table(src_stroke, tgt_stroke, colormatch_dic, verbose = False):
    try:
        # Check if src_stroke and tgt_stroke are valid
        if src_stroke is None or tgt_stroke is None:
            if verbose:
                print("Source or target stroke is None")
            return

        # Get the color of the source stroke
        src_color = src_stroke.material_index

        # Find the matching color in the color match dictionary
        tgt_color = colormatch_dic.get(src_color)

        if tgt_color is not None and tgt_color != src_color:
            # If a matching color is found, set the target stroke's color to the matching color
            tgt_stroke.material_index = tgt_color
        else:
            # If no matching color is found, print a warning or handle the case as needed
            if tgt_color is None:
                if verbose:
                    print(f"No matching color found for source color {src_color}")
    except Exception as e:
        print(f"Error in colormatch_from_table: {e}")

def colormatch(src_ob, src_frame, ob, new_frame ) :
    '''Checks if check if the stroke uses same material as source stroke 
    assumes that new_frame is a fresh copy of src frame, but in a different object
    assumes that destination object has all the needed materials already in his list
    args : src_ob = source object
           src_frame = src Gpencil frame 
           ob = destination object
           new_frame = destination gpencil frame'''
    #start_time = time.time()
    matched_strokes = []
    if len(new_frame.strokes) > 0 and len(src_frame.strokes) > 0 :
        for i, stroke in enumerate(new_frame.strokes):
            #stroke_mat = gp_ob.materials[stroke.material_index] #WRONG : doesn't work if there are empty slots
            
            try :
                src_stroke = src_frame.strokes[i] # TOCHECK: why is src_frame.strokes not the same length as new_frame.strokes ?
                stroke_mat = ob.material_slots[stroke.material_index].material
            except IndexError :
                stroke_mat = None

            #src_stroke_mat = src_gp_ob.materials[src_stroke.material_index]
            if len(src_ob.material_slots) > 0 :
                src_stroke_mat = src_ob.material_slots[src_stroke.material_index].material
            else :
                src_stroke_mat = None

            if src_stroke_mat :
                if stroke_mat == None or stroke_mat.name != src_stroke_mat.name :
                    #print('color dont match ! ' + src_stroke_mat.name + ' - ' + stroke_mat.name)
                    matched_strokes.append(stroke)

                    for index, material_slot in enumerate(ob.material_slots) :
                        material = material_slot.material
                        if material is not None :
                            if material.name == src_stroke_mat.name or material.name == src_stroke_mat.name :
                                stroke.material_index = index
                                #print(str(i) + ' - colormatching : ' + src_stroke_mat.name + ' - ' +  material.name )
            if src_stroke_mat == None :
                stroke.material_index = -1
    #print('COLORMATCH : relocated ' + str(len(matched_strokes)) + ' strokes material : ', time.time() - start_time )                    
    return {'FINISHED'}


def undo_gp_layer_matrix_transform(gp_object):
    '''Patch to undo the transformation of Grease Pencil layer's matrix 
        applied to each points on duplication'''
    # Create an identity matrix for comparison
    identity_matrix = Matrix.Identity(4)

    # Iterate through all layers
    for layer in gp_object.data.layers:
        # Check if the layer's matrix is different from the identity matrix
        if layer.matrix_layer != identity_matrix:
            # Store the original layer's matrix
            original_matrix = layer.matrix_layer.copy()

            # Invert the layer's matrix
            inv_matrix = original_matrix.inverted()

            # Iterate through all frames and strokes
            for frame in layer.frames:
                for stroke in frame.strokes:
                    # Apply the inverted matrix to each point's coordinates
                    for point in stroke.points:
                        point.co = inv_matrix @ point.co

                    # Reset the layer's matrix to avoid double transformation
                    #layer.matrix = original_matrix


def stroke_interpolation(tgt_stroke, source_stroke_list, coeff_list, verbose = False):
    """calculate a pseudostroke from a list of strokes and weights"""
    #Caclulate the number of points in the interpolated stroke
    length = 0
    for ind,stroke in enumerate(source_stroke_list):
        if stroke is None:
            continue
        length+=len(stroke.points)*coeff_list[ind]
    
    length = int(length)
    # Instanciate pseudo_stroke
    stroke = Pseudo_stroke()
    stroke.points.add(count = length)

    # Interpolate points
    for l, point in enumerate(stroke.points) : 
        update = True   
        # tgt_point_array=[]
        point_list = []
        weight_list = []
        total_weight = 0
        for ind,src_stroke in enumerate(source_stroke_list):
            src_point = None
            if src_stroke is None or len(src_stroke.points)==0:
                continue
            if len(src_stroke.points)>l:
                src_point = src_stroke.points[l]
                # tgt_point_array.append(src_stroke.points[l])
            else:
                previous_index = l-1
                while previous_index >= 0:
                    if len(src_stroke.points)>previous_index:
                        src_point = src_stroke.points[previous_index]
                        break
                    previous_index -= 1
                

            if point is not None:
                point_list.append(src_point)
                weight_list.append(coeff_list[ind])
                    # Add the weight to total_weight
                total_weight += coeff_list[ind]
            
        if update:
            # point_list = [tgt_point_array[i].co * coeff_list[i] 
            #                 for i in range(len(coeff_list))]
            if len(point_list) == 0 or total_weight <= 0.5:
                # remove points over this index (assuming that alkl folowing points wil also have to be deleted)
                for point_ind in reversed( range(l, len(stroke.points))):
                    stroke.points.pop(point_ind)
                break

            final_co = Vector((0,0,0))
            final_pressure = 0
            final_strength = 0
            for ind,p in enumerate(point_list):
                final_co += p.co * weight_list[ind]
                final_pressure += p.pressure * weight_list[ind]
                final_strength += p.strength * weight_list[ind]
            
            final_co /= total_weight
            
            point.co = final_co.copy()   
            point.pressure = final_pressure/total_weight      
            point.strength = final_strength/total_weight                                 
                                        
    #conform src-stroke to pseudo_stroke
    stroke_confo(stroke, tgt_stroke, keep=True, verbose = False,
                                point_attributes=['co','pressure','strength'],
                                stroke_attributes=[])

def layer_interpolation(tgt_layer, source_layer_list, coeff_list, verbose = False):
    """interpolate a layer from a list of layers and weights"""
    #Caclulate the number of strokes in the interpolated layer
    length = 0
    for ind,layer in enumerate(source_layer_list):
        if layer is None:
            continue
        length+=len(layer.strokes)*coeff_list[ind]
    
    length = round(length)
    # Add stroke to the target layer if there isn't enough
    if length > len(tgt_layer.strokes):
        for i in range(length - len(tgt_layer.strokes)):
            
            #TODO remove Ugly workaround
            #get any stroke from the source layer list
            src_stroke = None
            #WARNING this do assume that there isn't 2 different materials in a single layer
            #However the whole interpolation system is based on the fact that all layers have only one material
            for source_layer in source_layer_list:
                if source_layer is not None:
                    if len(source_layer.strokes)>0:
                        src_stroke = source_layer.strokes[0]
                    break
            if src_stroke is None:
                break
            new_stroke = tgt_layer.strokes.new()
            stroke_confo(src_stroke, new_stroke,conform_points=False, verbose = False)
            
    elif length < len(tgt_layer.strokes):
        for i in range(len(tgt_layer.strokes) - length):
            tgt_layer.strokes.remove(tgt_layer.strokes[-1])
    
    for k, stroke in enumerate(tgt_layer.strokes ):
                        

        stroke_list = [gp.strokes[k] if len(gp.strokes) > k else None for gp in source_layer_list  ]   

        #FACTORIZATION OF OLD
        stroke_interpolation(stroke, stroke_list, coeff_list, verbose = False)    